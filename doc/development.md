
## Dataset Import/Export

To populate the different Doctypes with a set of demo data the script `dataset_tool.py` can be used. It allows to export and import a series of Doctypes to csv. It is basically a wrapper around the `bench` command, so make sure it is available on the command line.

### Requirements

* Install yaml package with `pip3 install pyyaml`

### Load dat    aset

To load the demo data set, proceed as follows:

1. `cd uppsala`
2. `python3 dataset_tool.py import config.yaml` where `config.yaml` holds a list of all Doctypes that should be imported.

### Save dataset

Similarly, run `python3 dataset_tool.py export config.yaml` to save the current data to disk.