# Handbuch

Uppsala ist eine Schulverwaltungssoftware mit Fokus auf Höhere Fachschulen in der Schweiz. Die Konzepte sollten sich allerdings ebenfalls auf Seminar, Universitäten und Volksschulen anwenden lassen.

## Ziele

* Verwaltung der Kopfdaten von Studierenden.
* Automatische Erzeugung von Studienbestätigungen als PDF.
* Abbildung des administrativen Schuljahrs mit Semestern, Fächern, Klassen und Lektionen
* Rückverfolgbarkeit der schulischen Laufbahn von Studierenden (Liste aller eingeschriebenen Semester)
* Erfassung der Noten und Leistungsnachweisen der Studierenden
* Erfassung von An- und Abwesenheiten der Studierenden
* Automatische Erzeugung von Zeugnissen als PDF unter Einbezug der Noten und Anwesenheiten.

## Einführung Datenbankmodell

Die oben erfassten Ziele lassen sich mit folgendem Datenbankschema. Es ist ratsam, sich  mit der Struktur der Daten vertraut zu machen.

[Datenbankmodell](./imgs/database_scheme.pdf)

Der administrative Schulbetrieb wird mit folgenden drei Konzepten abgebildet:

1. Semester (*Semester*) und Semesterdurchführungen (*Semester Execution*): Einschreibung von Studierenden in Semesterdurchführungen erlaubt die Rückverfolgbarkeit der schulischen Laufbahn. 
2. Fächer (*Course*) und Fächerdurchführungen (*Course Execution*): Zuweisung von Fächerdurchführungen zu Semesterdurchführungen erlaubt die Erfassung von Noten
3. Lektionen (*Lecture*) und Lektionsdurchführungen (*Lecture Execution*): Erlaubt die Erfassung von Anwesenheiten

Das Semester (*Semester*) wie auch die Lektion (*Lecture*) beschreiben dabei Zeiträume. In der Semesterdurchführung (*Semester Execution*) wird ein Semester (*Semester*) mit einer Schulklasse (*School Class*) verknüpft.
Mittels einer Einschreibung (*Enrollment*) wird nun eine Studierende (*Student*) mit einer Semesterdurchführung (*Semester Execution*) verknüpft. Eine schulische Laufbahn einer Studierenden besteht aus einer Liste von Einschreibungen (*Enrollment*). Muss der Studierende (*Student*) ein Semester wiederholen, weil die Steigungsbedingungen nicht erfüllt werden, so wird er anfangs des neuen Semesters in die Semesterdurchführung (*Semester Execution*) einer anderen Klasse (*School Class*) eingeschrieben (*Enrollment*).

Durch eine Fächerdurchführung (*Course Execution*) wird ein Fach (*Course*) mit einer Semesterdurchführung verknüpft (*Semester Exection*). Fächer (*Course*) werden somit einmal angelegt und dann immer wieder durchgeführt.

Die Leistungsnachweise und Noten betreffen immer eine bestimmte Durchführung eines Fachs (*Course Exection*) und einen betreffenden Student (*Student*).

Eine Lektion (*Lecture*) ist ein Zeitraum, üblicherweise 45 Minuten. Die Lektionsdurchführung (*Lecture Execution*) bildet eine Lektion realen Unterricht ab: Ein Fach (Fächerdurchführung) findet in einer Lektion statt.

Anwesenheiten (*Attendance*) verknüpfen schliesslich eine Studentin (*Student*) mit einer Lektionsdurchführung (*Lecture Execution*). Im Schulbetrieb wird somit für jede Studierende in jeder Lektion eine Anwesenheit (*Attendance*) angelegt.


Die Kopfdaten der Studierenden ist in der Tabelle *student* abgespeichert.


## FAQ

### Wie finde ich heraus, in welcher Klasse eine Schülerin ist?

Filtere die Tabelle Einschreibung (*Enrollment*) nach der Schülerin. Es erscheint eine Liste mit allen Einschreibungen. Die Einschreibungen sind via Semesterdurchführung (*Semester Execution*) mit einer Schulklasse (*School Class*) verknüpft. Da Einschreibung hat zudem die Klasse im Namen. Somit ist es nicht nötig, diese Verknüpfung aufzulösen.

### In welchem Semester ist die Klasse, welche das Studium im Jahr 2022 begonnen hat?

Filtere die Tabelle Semesterdurchführung (*Semester Execution*) nach der Schulklasse (*School Class*) mit Startdatum 2022. Das Feld *Level* in der Tabelle *Semester Execution* zeigt an, ob es sich bei dieser Semesterdurchführung um das erste, zweite usw. Semester handelt. In einem Bachelorstudium mit 6 Semestern wäre das Feld *Level* also eine Ganzzahl zwischen 1 und 6.

### Wie wird ein Seminar abgebildet?

* Ein Seminar ist ein Semester (*Semester*).
* Eine Seminardurchführung ist eine Semesterdurchführung (*Semester Execution*).
* Der Seminarinhalt ist ein Fach (*Course*). Hier wird auch der Name der Dozentin hinterlegt.
* Die Durchführung des Seminarinhalts ist die Fächerdurchführung (*Course Exection*).
* Für jede Teilnehmerinnen (*Student*) wird eine Einschreibung angelegt.

### Welche Noten hatte Student Bob im dritten Semester?

Dafür lässt sich das entsprechende Zeugnis (*Grade Report*) von Bob suchen.

### Wie wird ein Zeugnis erstellt?

Bei der Erstellung eines Zeugnisses wird im Hintergrund folgender Prozess abgewickelt:

1. Liste aller Fächer erstellen, indem die Tabelle Fächerdurchführung (*Course Execution*) nach der entsprechenden Semesterdurchführung (*Semester Execution*) gefiltert wird.
2. Für jedes Fach der Fächerliste die Tabelle Note (*Grade*) filtern nach dem entsprechenden Student (*Student*) und der Fächerdurchführung *Semester Exection*.

Der Student (*Student*) und die Semesterdurchführung (*Semester Execution*) sind im Zeugnis (*Grade Report*) via die Felder *enrollment.student* und *enrollment.semester_execution* verfügbar.
