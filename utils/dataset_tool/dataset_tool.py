#!/usr/bin/python

import subprocess
import argparse
from pathlib import Path
import json
import yaml
import os.path
import logging
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)

def export_doctypes(doctypes, path_data, format, site=None):
    path_data = Path(path_data)
    if not path_data.exists():
        path_data.mkdir(parents=True)

    for doctype in doctypes:
        logging.info(f"Exporting {doctype}")
        command = ["bench"]
        if site is not None:
            command += ["--site", site] 
        command += [f"export-{format}", doctype, str(path_data/f"{doctype}.{format}")]
        ret = subprocess.run(args=command)
        logging.debug(f"Running {command}: {ret}")

def import_doctypes(doctypes, path_data, format, site=None):
    if format == "json":
        subcmd = "import-doc"
    elif format == "csv":
        subcmd = "import-csv"

    for doctype in doctypes:
        logging.info(f"Importing {doctype}")

        command = ["bench"]
        if site is not None:
            command += ["--site", site] 
        command += [subcmd, str(path_data/f"{doctype}.{format}")]
        ret = subprocess.run(args=command)
        logging.debug(f"Running {command}: {ret}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Import/Export tool for frappe bench dummy dataset')
    parser.add_argument('-v','--verbosity', type=int, help='Verbosity level')
    parser.add_argument('--site', type=str, help='Site to operate on. If not given, bench will use the default site from sites/currentsite.txt')
    parser.add_argument('command', type=str, help='Command: import or export')
    parser.add_argument('config', type=str, help='list of all doctypes to export')

    args = parser.parse_args()

    configs = list()
    if not Path(args.config).exists():
        logging.error(f"Config file {args.config} doesn't exists.")
        exit(1)
    with open(args.config, 'r') as file:
        try:
            configs = yaml.load(file, Loader=yaml.SafeLoader)
        except e:
            logging.error(e)
            exit(1)
    site = None
    if args.site:
        site = args.site
    for config in configs:
        if "path_data" not in config:
            logging.warning("path_datasetting not found in config.")
        path_data= Path(os.getcwd())/config["path_data"]
        if "doctypes" not in config:
            logging.error("doctypes setting (list) not found in config.")
            exit(1)

        if args.command == "export":
            export_doctypes(doctypes=config['doctypes'], path_data=path_data, format=config['format'], site=site)
        elif args.command == "import":
            import_doctypes(doctypes=config['doctypes'],  path_data=path_data, format=config['format'], site=site)
        else:
            parser.print_help()

