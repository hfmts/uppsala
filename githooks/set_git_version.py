import subprocess
import pathlib

def get_git_describe() -> str:
    cmd = ['git', 'describe']
    cwd = pathlib.Path(__file__).parent.absolute()
    ret = subprocess.run(cmd, capture_output=True, cwd=cwd)
    version = ret.stdout.decode('ascii').strip()
    return version

def set_git_describe():
    version = get_git_describe()
    print(f"Setting app version to {version}")
    path_root = pathlib.Path(__file__).parent.absolute().parent
    path_out = path_root/"uppsala/__init__.py"
    with open(path_out, "w") as file:
        content = f"__version__ = '{version}'"
        file.write(content)

if __name__ == "__main__":
    set_git_describe()