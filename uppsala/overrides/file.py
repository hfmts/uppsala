import os
import frappe
import frappe.core.doctype.file.utils

from pathlib import Path

PATH_FILESTORE = "/workspace/filestore"
PATH_FMT = "{term}/{doctype}/{level}/{filename}.pdf"

def compose_file_path(doctype:str, docname:str) -> tuple[bool, Path]:
    """
        determine if this document should be mirrored and what the 
        mirror path in the external file store should be
    """
    path = Path(PATH_FILESTORE)
    mirror = False
    doc_rec = frappe.get_doc(doctype, docname)
    if doctype in ["Generic Bill"]:
        mirror = True
        filename = f"{doc_rec.name}"
        path /= "Generic Bill"
        path /= filename
    elif doctype in ["Enrollment Confirmation", "Student Bill", "Grade Report"]:
        mirror = True
        doc_enrollment = frappe.get_doc("Enrollment", doc_rec.enrollment)
        term = doc_enrollment.term.replace("/", "_")
        filename = f"{doc_rec.name}_{doc_enrollment.student_last_name}_{doc_enrollment.student_first_name}"
        path /= PATH_FMT.format(doctype=doctype, term=term, level=doc_enrollment.level, filename=filename)
    else:
        mirror = False
    return (mirror, path)

# will override the implementation of writing file to disk
# can be used to upload files to a CDN instead of writing
# the file to disk
def write_file(file, *args, **kwargs):
    # print("--------write_file hook--------")
    # print(f"{file.file_name=} {file.attached_to_doctype=} {file.attached_to_name=}")
    (do_mirror, path_file) = compose_file_path(file.attached_to_doctype, file.attached_to_name)
    if do_mirror:
        path_file.parent.mkdir(parents=True, exist_ok=True)
        with open(path_file, 'wb') as file_out:
            file_out.write(file.content)

    return file.save_file_on_filesystem()



# will override the implementation of deleting file from disk
# can be used to delete uploaded files from a CDN instead of
# deleting file from disk
def delete_file(file, *args, **kwargs):
    # print("--------delete_file hook--------")
    # print(f"{args=} {kwargs=}")
    # print(f"{file.file_name=} {file.attached_to_doctype=} {file.attached_to_name=} {file.file_url=}")
    (is_mirrored, path_file) = compose_file_path(file.attached_to_doctype, file.attached_to_name)
    if is_mirrored:
        path_file.unlink(missing_ok=True)

    frappe.core.doctype.file.utils.delete_file(file.file_name)
