// frappe.form.link_formatters['School Class'] = function(value, doc, docfield) {
//     // NOTE: this is no good cuz 1. the returned value is not used
//     // and 2. the doc is not the school class doc but the doc where the link is used,
//     console.log("Link formater" + value + doc + docfield);
//     let name = "School Class Link Formatter";
//     return name;
//     // if(doc.name_short && doc.name_long) {
//     //     return doc.name_short + doc.name_long;
//     // } else {
//     //     return value + '1';
//     // }
// }

const RPCResponseCode = {
  SUCCESS:0,
  FAIL:1
};



function list_get_checked(doctype) {
    // get selected items
    let listview = frappe.get_list_view(doctype);
    let items = listview.get_checked_items();
    let names = [];
    for (i in items) {
        names.push(items[i].name);
    }

    return names;
}

function list_merge_pdfs(doctype) {
    console.log(`Merging pdfs for ${doctype}`);
    let names = list_get_checked(doctype);
    merge_pdf(doctype, names).then(() => {});
}

function merge_pdf(doctype, items) {
    console.log(`Merging pdf for ${doctype}: ${items}`);
	  const w = window.open(
	  	"/api/method/uppsala.core.store_pdf.merge_pdf?" +
	  		"doctype=" +
	  		encodeURIComponent(doctype) +
	  		"&items=" +
	  		encodeURIComponent(JSON.stringify(items))
    );

	  if (!w) {
	  	frappe.msgprint(__("Please enable pop-ups"));
	  	return;
	  }
}


function list_store_pdfs(doctype) {
    console.log(`Storing pdfs for ${doctype}`);
    let names = list_get_checked(doctype);
    store_pdf(doctype, names).then(() => {});
}

function store_pdf(doctype, items) {
    console.log(`Storing pdf for ${doctype}: ${items}`);

    let promise = new Promise(function(resolve, reject) {
        frappe.call({
            method: 'uppsala.core.store_pdf.store_pdf',
            freeze: true,
            args: {
                doctype: doctype,
                items: items
            }
        })
        .then((r) => {
            let msg = r.message;
            console.log(msg);
            if (msg.meta.code == RPCResponseCode.SUCCESS) {
                let infomsg = __("Successfully created {0} {1} print pdfs!", [items.length, doctype]);
                frappe.show_alert({message: infomsg, indicator: "green"}, 10);
            }
            else {
                let infomsg = __("Error creating {0} print pdfs, only created {1} {0} print pdfs!", [doctype, items.length]);
                frappe.show_alert({message: infomsg, indicator: "red"}, 20);
            }
            resolve();
        });
    });
  return promise;
}

function update_participants(frm, field_name_table) {
    console.log("loading participants", frm);
    frappe.call({
        // method: 'uppsala.uppsala.doctype.enrollment.enrollment.get_level_up_values',
        method: 'uppsala.core.utils.get_participants',
        freeze: true,
        args: {
            course_execution: frm.doc.course_execution
        }
    }).then(r => {
        if (r.message) {
            let msg = r.message;
            console.log(msg)
            // go over all students
            for (let i in msg.data) {

                // check if entry for student exists
                let exists = false;
                for (let j in frm.doc[field_name_table]) {
                    if (frm.doc[field_name_table][j].student == msg.data[i].student) {
                        exists = true;
                        break;
                    }
                }
                if (!exists) {
                    let row = frm.add_child(field_name_table, {
                        student: msg.data[i].student,
						// this value might be overwritten by auto-fetch on save...
                        student_last_name: msg.data[i].student_last_name,
                        student_first_name: msg.data[i].student_first_name,
                    });
                }
            }
            frm.refresh_field(field_name_table);
        }
    })
}

function update_assessments(frm, enrollment) {
    console.log(enrollment);
    frm.call('get_course_scores').then(r => {
        let getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

        if (r.message) {
            let msg = r.message;
            let rows = '';
            // go over all course/grades
            for (let i in msg.data) {

                for(let j in msg.data[i].assessments) {
                    rows += 
                    `<div class="grid-row">
                    <div class="data-row row">
                        <div class="col grid-static-col col-xs-4">
                            <div class="static-area ellipsis"><a href="${baseUrl}/course-execution/${msg.data[i].course_execution}">${msg.data[i].course_description}</a></div>
                        </div>
                       <div class="col grid-static-col col-xs-2">
                            <div class="static-area ellipsis"><a href="${baseUrl}/assessment/${msg.data[i].assessments[j].name}">${msg.data[i].assessments[j].weight}</a></div>
                        </div>
                        <div class="col grid-static-col col-xs-3">
                            <div class="static-area ellipsis"><a href="${baseUrl}/assessment/${msg.data[i].assessments[j].name}">${msg.data[i].assessments[j].grade_value}</a></div>
                        </div>
                        <div class="col grid-static-col col-xs-3"></div>
                    </div>
                    </div>`;
                }
                rows += 
                `<div class="grid-row">
                <div class="data-row row">
                    <div class="col grid-static-col col-xs-4">
                        <div class="static-area ellipsis"><b><a href="${baseUrl}/course-execution/${msg.data[i].course_execution}">${__('Score')} ${msg.data[i].course_description}</a></b></div>
                    </div>
                    <div class="col grid-static-col col-xs-2"></div>
                    <div class="col grid-static-col col-xs-3">
                        <div class="static-area ellipsis"><b>${msg.data[i].grade_final} (${msg.data[i].grade_final_raw})</b></div>
                    </div>
                    <div class="col grid-static-col col-xs-3">
                        <div class="static-area ellipsis">
                            <b><a href="${baseUrl}/attendance/${msg.data[i].attendance.name}">${msg.data[i].attendance.value}</a></b>
                        </div>
                    </div>
                </div>
                </div>`;
            }
            frm.get_field('grade_summary').$wrapper.html(`
                <div class="form-grid">
                    <div class="grid-heading-row">
                    <div class="grid-row">
                    <div class="data-row row">
                        <div class="col grid-static-col col-xs-4">
                            <div class="static-area ellipsis">${__('Course Description')}</div>
                        </div>
                        <div class="col grid-static-col col-xs-2">
                            <div class="static-area ellipsis">${__('Assessment Weight')}</div>
                        </div>
                        <div class="col grid-static-col col-xs-3">
                            <div class="static-area ellipsis">${__('Assessment Grade')}</div>
                        </div>
                        <div class="col grid-static-col col-xs-3">
                            <div class="static-area ellipsis">${__('Attendance')}</div>
                        </div>
                    </div>
                    </div>
                    </div>
                    ${rows}
                </div>
            `);
        }
    });
}


function get_colors_level() {
  
  frappe.db.get_list('Level', {
      fields: ['name', 'color'],
      filters: {}
  }).then(records => {
      let colmap_level = new Object();
      for (let i in records) {
        colmap_level[records[i].name] = records[i].color;
      }

      localStorage.setItem("colmap_level", JSON.stringify(colmap_level));
  })
}

function get_col_level(val) {
  let cols = JSON.parse(localStorage.getItem("colmap_level"));
  let color = "grey";
  if (cols.hasOwnProperty(val)) {
    color = cols[val];
  }
  return `<span class="indicator-pill ${color} filterable ellipsis" data-filter="level,=,${val}"> ${val} </span>`;
}
 
