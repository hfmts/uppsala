frappe.form.link_formatters['student'] = function(value, doc) {
    if(doc.full_name && doc.full_name !== value) {
        return value + 'aight' + doc.full_name;
    } else {
        return value + 'aigh';
    }
}
