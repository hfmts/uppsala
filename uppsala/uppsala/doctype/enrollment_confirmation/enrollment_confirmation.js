// Copyright (c) 2023, HFMTS and contributors
// For license information, please see license.txt

frappe.ui.form.on('Enrollment Confirmation', {
  refresh(frm) {
      frm.add_custom_button('Store PDF', () => {
        let items = new Array(frm.docname)
        store_pdf(frm.doctype, items).then(() => {
	        frm.sidebar.reload_docinfo();
          frm.refresh();
        });
      });
  },

  onload(frm) {
      set_defaults(frm);
  }
});

function set_defaults(frm) {
    if (typeof frm.doc.issued_by == 'undefined') {
        frappe.call({
            method: 'uppsala.uppsala.doctype.enrollment_confirmation.enrollment_confirmation.get_defaults',
            freeze: true,
        })
        .then((r) => {
            let msg = r.message;
            console.log(msg);
            frm.set_value('issued_by', msg.issued_by);
       });
    }
}

