# Copyright (c) 2023, HFMTS and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document


@frappe.whitelist()
def get_defaults():
    """ Get default issued_by"""
    msg = dict()
    signature = frappe.get_doc("School Settings").signature_enrollment_confirmation
    msg['issued_by'] = signature
    return msg
    

class EnrollmentConfirmation(Document):
    
    def before_save(self):
        msg = get_defaults()
        if self.issued_by == None:
            self.issued_by = msg["issued_by"]
