// Copyright (c) 2021, HFMTS and contributors
// For license information, please see license.txt

frappe.ui.form.on('Assessment', {
 	refresh: function(frm) {
        frm.add_custom_button('Load participants', () => {
            update_participants(frm, "student_assessments");
        })
    },
    // frm passed as the first parameter
    course_execution(frm) {
        update_participants(frm, "student_assessments");
    },
    before_save(frm) { 
        validate_grades(frm);
    }
});

function validate_grades(frm) {
    console.log(frm.doc.type);
    if (frm.doc.type == 'Number') {
        let bad_grades = [];
        for (let i in frm.doc.student_assessments) {
            if (isNaN(frm.doc.student_assessments[i].grade_value)) {
                bad_grades.push(i);
            }
        }
        if (bad_grades.length > 0) {
            let rows = ""
            for (let i in bad_grades) {
                rows += 
                `<tr>
                    <td>${frm.doc.student_assessments[bad_grades[i]].student}</td>
                    <td>${frm.doc.student_assessments[bad_grades[i]].student_last_name}</td>
                    <td>${frm.doc.student_assessments[bad_grades[i]].student_first_name}</td>
                    <td>${frm.doc.student_assessments[bad_grades[i]].grade_value}</td>
                </tr>`;
            }
            let msg = `
            You are trying to add text to an assessment of type ${__('Number')}. Please change to type ${__('Text')} or check the following entries and try again: </br>
            <table>
                <tr>
                    <th>${__('Student')}</th>
                    <th>${__('Last Name')}</th>
                    <th>${__('First Name')}</th>
                    <th>${__('Grade')}</th>
                </tr>
                ${rows}
            </table>
            `;
            frappe.throw(msg);
        }
    }
}