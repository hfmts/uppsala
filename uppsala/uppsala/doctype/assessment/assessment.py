# Copyright (c) 2021, HFMTS and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

frappe.utils.logger.set_log_level("DEBUG")
logger = frappe.logger("uppsala")

class Assessment(Document):

    def validate(self):
        course_execution = frappe.get_doc("Course Execution", self.course_execution)
        logger.debug(f"Validating if assessment for {course_execution.name} is allowed.")
        if course_execution.nb_of_assessments == "none":
            frappe.throw(f"The referenced course execution is a course without assessments. Change the number of assessments in {course_execution.name} first.")
        elif course_execution.nb_of_assessments == "one":
            assessments = frappe.get_all("Assessment", filters={"course_execution": course_execution.name})
            if len(assessments) > 0:
                frappe.throw(f"The referenced course execution is a course with one assessment and it already has an assessment ({assessments[0]['name']}). Change the number of assessments in {course_execution.name} first.")

