// Copyright (c) 2022, HFMTS and contributors
// For license information, please see license.txt

frappe.ui.form.on('Grade Report', {
  refresh(frm) {
      frm.add_custom_button('Store PDF', () => {
        let items = new Array(frm.docname)
        store_pdf(frm.doctype, items).then(() => {
	        frm.sidebar.reload_docinfo();
          frm.refresh();
        });
      });
  },

  onload(frm) {
      set_defaults(frm);
      grade_report_update_assessments(frm);
  },
  load_assessments(frm) {
      grade_report_update_assessments(frm);
  },
  enrollment(frm) {
      grade_report_update_assessments(frm);
  },

});

function grade_report_update_assessments(frm) {
      let enrollment = frm.get_field("enrollment");
      if (typeof frm.doc.enrollment !== 'undefined') {
        update_assessments(frm, enrollment);
      }
}

function set_defaults(frm) {
    if (typeof frm.doc.issued_by == 'undefined') {
        frappe.call({
            method: 'uppsala.uppsala.doctype.grade_report.grade_report.get_defaults',
            freeze: true,
        })
        .then((r) => {
            let msg = r.message;
            console.log(msg);
            frm.set_value('issued_by', msg.issued_by);
       });
    }
}

