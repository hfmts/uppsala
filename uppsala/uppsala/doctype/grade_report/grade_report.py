# Copyright (c) 2022, HFMTS and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from uppsala.core.rpc import RPCResponse, RPCResponseCode

logger = frappe.logger("uppsala")

@frappe.whitelist()
def bulk_create_grade_reports(report_type:str, enrollments:str) -> RPCResponse:
    """level up items"""
    import json
    items = json.loads(enrollments)

    data = list()
    print(f"bulk creating grade reports for {items}")
    for item in items:
        vals = dict()
        vals["doctype"] = "Grade Report"
        vals["report_type"] = report_type
        vals["enrollment"] = item["name"]
        doc = frappe.get_doc(vals)
        doc.insert()
        # name = doc.level_up(term, level, load_course_executions)
        data.append(doc.name)

    if len(items) >= len(data):
        return { "meta": { "code": RPCResponseCode.SUCCESS}, "data": data }
    return { "meta": { "code": RPCResponseCode.FAIL, "msg": "Failed to create grade reports for some items"}}

@frappe.whitelist()
def get_defaults():
    """ Get default issued_by"""
    msg = dict()
    signature = frappe.get_doc("School Settings").signature_grade_report
    msg['issued_by'] = signature
    return msg

class GradeReport(Document):

    @frappe.whitelist()
    def get_course_scores(self, only_final=False):
        enrollment = frappe.get_doc("Enrollment", self.enrollment)
        return enrollment.get_course_scores(only_final)
    
    def before_save(self):
        msg = get_defaults()
        if self.issued_by == None:
            self.issued_by = msg["issued_by"]

    # def on_trash(self):
    #     filters = {
    #             "attached_to_doctype": self.__class__.__name__, 
    #             "attached_to_name": self.name 
    #     }
    #     files = frappe.get_all("File", filters=filters)
    #     print(f"Deleting {self.name}, also deleting {len(files)} related stored files")
