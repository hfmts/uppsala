frappe.listview_settings['Grade Report'] = {

    onload(listview) {
      // hides print action menu point
      listview.page.actions[0].childNodes[6].hidden = true;
      listview.page.add_action_item(__('Store PDFs'), () => list_store_pdfs('Grade Report'));
      listview.page.add_action_item(__('Merge PDFs'), () => list_merge_pdfs('Grade Report'));
    },
    // format how a field value is shown
    formatters: {
        report_type(val) {
          if (val == "Semester") {
            return `<span class="indicator-pill green filterable ellipsis"><span class="ellipsis"> ${val}</span></span>`;
          }
        }
    }
}
