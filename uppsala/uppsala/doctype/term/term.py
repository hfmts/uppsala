# Copyright (c) 2021, HFMTS and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from uppsala.core.rpc import RPCResponse, RPCResponseCode

# searches for leads which are not converted
@frappe.whitelist()
@frappe.validate_and_sanitize_search_inputs
def term_query(doctype, txt, searchfield, start, page_len, filters):
    '''
        List ordered by order and do not show archived!
    '''
    # return frappe.db.sql("""
    #     SELECT name, description
    #     FROM `tabTerm`
    #     WHERE `archive` = 0
    #     ORDER BY `ordinal_number`
    # """
    # )
    return frappe.db.sql("""
        SELECT name, description
        FROM `tabTerm`
        ORDER BY archive ASC, ordinal_number ASC;
    """
    )

@frappe.whitelist()
def get_last_ordinal_number() -> RPCResponse:
    docs = frappe.get_all('Term', fields=['ordinal_number'])
    num = 0
    for doc in docs:
        if doc["ordinal_number"] > num:
            num = doc["ordinal_number"]

    return {"meta": { "code": RPCResponseCode.SUCCESS }, "data": num }

class Term(Document):

    def get_next(self):
        docs = frappe.get_all('Term', filters={'ordinal_number': self.ordinal_number+1})
        if len(docs) == 1:
            return docs[0]['name']

