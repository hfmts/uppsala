// Copyright (c) 2021, HFMTS and contributors
// For license information, please see license.txt

frappe.ui.form.on('Term', {

    onload(frm) {
			console.log("Getting next orderinal number");
      set_ordinal_next(frm);
    },
});

function set_ordinal_next(frm) {
	if (frm.is_new()) {
			frappe.call({
	      method: 'uppsala.uppsala.doctype.term.term.get_last_ordinal_number',
	      args: {}
	  })
	  .then((r) => {
	      let msg = r.message;
	      console.log(msg);
	      if (msg.meta.code == RPCResponseCode.SUCCESS) {
	      	frm.set_value("ordinal_number", msg.data+1);
				}
	   });
	}
}
