# Copyright (c) 2021, HFMTS and contributors
# For license information, please see license.txt

import frappe
from frappe.utils import add_months
from frappe.model.document import Document
import qrbill
import svgwrite
import uppsala.core.utils
from uppsala.core.rpc import RPCResponse, RPCResponseCode


@frappe.whitelist()
def bulk_create_student_bills(enrollments) -> RPCResponse:
    """level up items"""
    import json
    items = json.loads(enrollments)

    data = list()
    print(f"bulk creating student bills for {items}")
    for item in items:
        vals = dict()
        vals["doctype"] = "Student Bill"
        vals["enrollment"] = item["name"]
        ret = get_defaults(item["name"])
        vals["use_billing_address"] = ret["use_billing_address"]
        vals["positions"] = ret["positions"]
        vals["bill_title"] = ret["bill_title"]
        doc = frappe.get_doc(vals)
        doc.insert()
        # name = doc.level_up(term, level, load_course_executions)
        data.append(doc.name)

    if len(items) >= len(data):
        return { "meta": { "code": RPCResponseCode.SUCCESS}, "data": data }
    return { "meta": { "code": RPCResponseCode.FAIL, "msg": "Failed to create student bills for some items"}}

@frappe.whitelist()
def get_defaults(enrollment):
    """ Given a enrollment, check if billing address exists and construct the study fee position entry. Return as dict ready to be consumed in get_doc()"""
    msg = dict()
    enrollment = frappe.get_doc("Enrollment", enrollment)
    student = frappe.get_doc("Student", enrollment.student)
    term = frappe.get_doc("Term", enrollment.term)
    level = frappe.get_doc("Level", enrollment.level)

    msg['use_billing_address'] = student.has_billing_address
    msg['bill_title'] = "Bildungsgang Dipl. Medizintechniker:in HF"
    msg['additional_information'] = f"{term.name} - {level.description} - {student.first_name} {student.last_name}"
    msg['positions'] = list()
    desc = f"{frappe._('Study Fee')} {level.description} {term.description}"
    msg['positions'].append({"description": desc, "type": "fee_level", "cost": level.fee})
    return msg



class StudentBill(Document):

    def calculate_total(self):
        # Total row
        total_costs = 0
        if not hasattr(self, "positions"):
            return 0
        for position in self.positions:
            total_costs += position.cost

        return total_costs

    def before_save(self):
        school_bill_settings = frappe.get_doc("School Bill Settings")
        if self.refnum == None or self.refnum == "" or self.refnum == "0":
            self.refnum = school_bill_settings.new_refnum()

        # due date when changing date (eg. via bulk edit)
        self.bill_date_due = add_months(self.bill_date, 1)

        self.total_amount = self.calculate_total()


    @frappe.whitelist()
    def get_bill(self):
        school_bill_settings = frappe.get_doc("School Bill Settings")
        enrollment = frappe.get_doc("Enrollment", self.enrollment)
        student = frappe.get_doc("Student", enrollment.student)
        term = frappe.get_doc("Term", enrollment.term)
        level = frappe.get_doc("Level", enrollment.level)

        debtor = dict()
        debtor["country"] = "CH"
        if not self.use_billing_address:
            debtor["name"] = f"{student.first_name} {student.last_name}"
            debtor["street"] =  student.address_street
            debtor["house_num"] = student.address_number
            debtor["pcode"] = student.zip_code
            debtor["city"] = student.city
        else:
            debtor["name"] = student.address_bill_name
            debtor["street"] =  student.address_bill_street
            debtor["house_num"] = student.address_bill_number
            debtor["pcode"] = student.address_bill_zip_code
            debtor["city"] = student.address_bill_city

        bill = qrbill.QRBill(
            language='de',
            account= school_bill_settings.setting_bill_iban,
            creditor={ 'name': school_bill_settings.setting_bill_address_name,
                     "street":  school_bill_settings.setting_bill_address_street,
                     "house_num": school_bill_settings.setting_bill_address_number,
                     "pcode":  school_bill_settings.setting_bill_address_zip,
                     "city":   school_bill_settings.setting_bill_address_city,
                     "country": "CH" },
            # creditor= school_settings.billing_address,
            # amount=school_settings.billing_cost,
            amount=str(self.total_amount),
            debtor = debtor,
            extra_infos=self.additional_information,
            ref_number = self.refnum
        )
        # bill.as_svg('/workspace/frappe-bench/sites/mysite.localhost/public/files/bill.svg')
        # see QRBill.as_svg ...
        dwg = svgwrite.Drawing(
            size=(qrbill.bill.A4[0], f'{qrbill.bill.BILL_HEIGHT}mm'),  # A4 width, A6 height.
            viewBox=('0 0 %f %f' % (qrbill.bill.mm(qrbill.bill.A4[0]), qrbill.bill.mm(qrbill.bill.BILL_HEIGHT))),
        )
        dwg.add(dwg.rect(insert=(0, 0), size=('100%', '100%'), fill='white'))  # Force white background
        bill_group = bill.draw_bill(dwg)
        svg = dwg.tostring()
        return svg


