// Copyright (c) 2021, HFMTS and contributors
// For license information, please see license.txt

frappe.ui.form.on('Student Bill', {
    refresh(frm) {
        frm.add_custom_button('Store PDF', () => {
          let items = new Array(frm.docname)
          store_pdf(frm.doctype, items).then(() => {
		        frm.sidebar.reload_docinfo();
            frm.refresh();
          });
        });
    },
    enrollment(frm) {
        set_defaults(frm);
    },
    bill_date(frm) {
        // due date when changing date
        frm.set_value("bill_date_due", frappe.datetime.add_months(frm.doc.bill_date, 1));
    },
    calculate_total(frm) {
        // recalculate total if button pressed
        calculate_total(frm);
    },
    positions(frm) {
        // recalculate total if positions changed
        calculate_total(frm);
    },

});

function set_defaults(frm) {
    console.log(frm.doc.enrollment);
    frm.set_value("bill_date", frappe.datetime.nowdate());
    // due date for new bill
    frm.set_value("bill_date_due", frappe.datetime.add_months(frm.doc.bill_date, 1));

    if (typeof frm.doc.enrollment !== 'undefined') {
        frappe.call({
            // method: 'uppsala.uppsala.doctype.enrollment.enrollment.get_level_up_values',
            method: 'uppsala.uppsala.doctype.student_bill.student_bill.get_defaults',
            freeze: true,
            args: {
                enrollment: frm.doc.enrollment
            }
        })
        .then((r) => {
            let msg = r.message;
            console.log(msg);
            // use billing address, if it exists
            if (msg.use_billing_address) {
                frm.set_value('use_billing_address', true);
            }
            if (msg.positions) {
                let position = msg.positions[0];
                frm.add_child('positions', position);
                frm.refresh_field('positions');
                calculate_total(frm);
            }
            if (msg.bill_title) {
                frm.set_value('bill_title', msg.bill_title);
            }
            if (msg.additional_information) {
                frm.set_value('additional_information', msg.additional_information);
            }
        });
    }
}

function calculate_total(frm) {

    let total_cost = 0;
    // go over all positions
    for (let i in frm.doc.positions) {
        let pos = frm.doc.positions[i];
        total_cost += pos.cost;
    }

    frm.set_value("total_amount", total_cost);
}
