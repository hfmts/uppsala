# Copyright (c) 2021, HFMTS and Contributors
# See license.txt

import frappe
import unittest
import uppsala.core.utils

class TestStudentBill(unittest.TestCase):
    def setUp(self):
        pass

    def test_store_pdf(self):
        name = "BILL-000051"
        doc = frappe.get_doc("Student Bill", name)

        doc.store()

        path_root = uppsala.core.utils.get_root_path()
        path_file = path_root/f"private/files/student_bills/{name}.pdf"
