
frappe.listview_settings['Student Bill'] = {
    onload(listview) {
        let $btn = listview.page.set_secondary_action('Add multiple', () => add_multiple(listview), "arrow-up-right");
    },
 
}

function add_multiple(listview) {
    console.log("Adding");
    // MultiSelectDialog for individual child selection
    let d = new frappe.ui.form.MultiSelectDialog({
        doctype: "Enrollment",
        target: "Student Bill",
        setters: {
            school_class: null,
            term: null,
            level: null
            // status: null
        },
        add_filters_group: 1,
        // date_field: "transaction_date",
        allow_child_item_selection: 0,
        // child_fieldname: "items", // child table fieldname, whose records will be shown & can be filtered
        // child_columns: ["enrollment", "student_first_name"], // child item columns to be displayed
        // get_query() {
            // return {
                // filters: { docstatus: ['!=', 2] }
            // }
        // },
        action(selections, args) {
            console.log(selections); // list of selected item names
            d.dialog.hide();
        },
    });
}