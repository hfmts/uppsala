# Copyright (c) 2021, HFMTS and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
logger = frappe.logger("uppsala")

class SchoolSettings(Document):

    #this method will run every time a document is saved
    def on_update(self):
        """update defaults"""
        logger.info(f"Setting default Term to {self.current_term}")
        frappe.db.set_default("Term", self.current_term)

        # clear cache
        frappe.clear_cache()        
