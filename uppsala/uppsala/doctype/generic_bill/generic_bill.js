// Copyright (c) 2022, HFMTS and contributors
// For license information, please see license.txt

frappe.ui.form.on('Generic Bill', {
  refresh(frm) {
      frm.add_custom_button('Store PDF', () => {
        let items = new Array(frm.docname)
        store_pdf(frm.doctype, items).then(() => {
	        frm.sidebar.reload_docinfo();
          frm.refresh();
        });
      });
  },
});


