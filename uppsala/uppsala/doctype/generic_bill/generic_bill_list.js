frappe.listview_settings['Generic Bill'] = {

    onload(listview) {
      // hides print action menu point
      listview.page.actions[0].childNodes[6].hidden = true;
      listview.page.add_action_item(__('Store PDFs'), () => list_store_pdfs('Generic Bill'));
      listview.page.add_action_item(__('Merge PDFs'), () => list_merge_pdfs('Generic Bill'));
    },
}
