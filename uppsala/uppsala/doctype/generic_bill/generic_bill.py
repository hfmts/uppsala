# Copyright (c) 2022, HFMTS and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
import qrbill
import svgwrite

class GenericBill(Document):

    def before_save(self):
        school_bill_settings = frappe.get_doc("School Bill Settings")
        if self.refnum == None or self.refnum == "" or self.refnum == "0":
            self.refnum = school_bill_settings.new_refnum(typenum=2)


    @frappe.whitelist()
    def get_bill(self):

        school_bill_settings = frappe.get_doc("School Bill Settings")

        debtor = dict()
        debtor["country"] = "CH"
        debtor["name"] = self.debtor_address_name
        debtor["street"] = self.debtor_address_street
        debtor["house_num"] = self.debtor_address_number
        debtor["pcode"] = self.debtor_address_zip
        debtor["city"] = self.debtor_address_city

        bill = qrbill.QRBill(
            language='de',
            account= school_bill_settings.setting_bill_iban,
            creditor={ 'name': school_bill_settings.setting_bill_address_name,
                     "street":  school_bill_settings.setting_bill_address_street,
                     "house_num": school_bill_settings.setting_bill_address_number,
                     "pcode":  school_bill_settings.setting_bill_address_zip,
                     "city":   school_bill_settings.setting_bill_address_city,
                     "country": "CH" },
            # creditor= school_settings.billing_address,
            # amount=school_settings.billing_cost,
            amount=str(self.amount),
            debtor = debtor,
            extra_infos=self.additional_information,
            ref_number = self.refnum
        )
        # bill.as_svg('/workspace/frappe-bench/sites/mysite.localhost/public/files/bill.svg')
        # see QRBill.as_svg ...
        dwg = svgwrite.Drawing(
            size=(qrbill.bill.A4[0], f"{qrbill.bill.BILL_HEIGHT}mm"),  # A4 width, A6 height.
            viewBox=('0 0 %f %f' % (qrbill.bill.mm(qrbill.bill.A4[0]), qrbill.bill.mm(qrbill.bill.BILL_HEIGHT))),
        )
        dwg.add(dwg.rect(insert=(0, 0), size=('100%', '100%'), fill='white'))  # Force white background
        bill_group = bill.draw_bill(dwg)
        svg = dwg.tostring()
        return svg
