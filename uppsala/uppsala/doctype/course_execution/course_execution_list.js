frappe.listview_settings['Course Execution'] = {

    hide_name_column: true, // hide the last column which shows the `name`
    onload(listview) {

        get_colors_level();

        if (listview.page.fields_dict.term) {
            // order terms by "ordinal_number" field and archived...
            listview.page.fields_dict.term.get_query = function(_doc, _cdt, _cdn) {
                return {
                    query: "uppsala.uppsala.doctype.term.term.term_query",
                }
            };
        }
        // triggers once before the list is loaded
        listview.page.add_action_item(__('Reexecute'), function() {
            console.log('Reexecute');
            let listview = frappe.get_list_view('Course Execution');
            let items = listview.get_checked_items();
            let level_dft = items[0].level;
            let term_dft = items[0].term;
            // check if items have the same values for term and level
            let sane = true;
            let msg = "";
            for (i in items) {
                console.log(items[i]);
                if (term_dft != items[i].term) {
                    msg = "Selected items have different terms: " + term_dft + " and " + items[i].term;
                    sane = false;
                }
                if (level_dft != items[i].level) {
                    msg = "Selected items have different levels: " + level_dft + " and " + items[i].level;
                    sane = false;
                }
            }
            if (!sane) {
                // with options
                frappe.msgprint({
                    title: __('Item selection error'),
                    indicator: 'red',
                    message: msg
                });
                return;
            }
            // get next term
            frappe
            .call({
                method: 'uppsala.uppsala.doctype.course_execution.course_execution.get_next_level',
                freeze: true,
                args: {
                    docname: items[0].name  // assuming all selected items have same term and level
                }
            })
            .then((r) => {
                let msg = r.message;
                let term_new = msg['data']['term'];
                // finally present dialog
                open_reexecute_dialog(items, term_new);
                listview.refresh();
            });
        });
    },
    // format how a field value is shown
    formatters: {
        level(val) {
            return get_col_level(val);
        }
    }
}

function open_reexecute_dialog(items, term_dft) {
    let d = new frappe.ui.Dialog({
        title: 'Select term to reexecute',
        fields: [
            {
                label: 'Term',
                fieldname: 'term',
                fieldtype: 'Link',
                options: "Term",
                default: term_dft
            }
            ],
        primary_action_label: 'Submit',
        primary_action(values) {
            bulk_reexecute(items, values.term);
            d.hide();
        }
    });

    d.show();
}

function bulk_reexecute(items, term) {
    frappe.call({
        method: 'uppsala.uppsala.doctype.course_execution.course_execution.bulk_reexecute',
        freeze: true,
        args: {
            items: items,
            term: term
        }
    })
    .then((r) => {
        let msg = r.message;
        if (msg.meta.status == "success") {
            let infomsg = __("Successfully reexecuting {0} Course Executions!", [msg.data.length]);
            frappe.show_alert({message: infomsg, indicator: "green"}, 10);
            frappe.utils.play_sound("submit");
        }
        else {
            let infomsg = __("Error reexecuting some Course Executions. {0} Course executions reexecuted successfully!", [msg.data.length]);
            frappe.show_alert({message: infomsg, indicator: "red"}, 20);
            frappe.utils.play_sound("delete");
        }

        // finally select newly created course executions
        var csv = msg.data.join(',');
        console.log(`Moving to course_execution/list with filter showing only ${csv}`);
        frappe.route_options = { "name": ["in", csv] };
        frappe.set_route(["List", "Course Execution", "List"]);
    });
}
