# Copyright (c) 2021, HFMTS and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

@frappe.whitelist()
def bulk_reexecute(items, term):
    """reexecute items"""

    import json
    items = json.loads(items)

    msg = dict()
    msg['data'] = list()
    msg['meta'] = dict()

    for item in items:
        doc = frappe.get_doc('Course Execution', item['name'])
        name = doc.reexecute(term)
        msg['data'].append(name)

    if len(items) < len(msg['data']):
        msg['meta']['status'] = 'fail'
    else:
        msg['meta']['status'] = 'success'

    return msg

@frappe.whitelist()
def get_next_level(docname):
    msg = dict()
    msg['data'] = list()
    msg['meta'] = dict()
    doc = frappe.get_doc('Course Execution', docname)
    if doc == None:
        msg['meta']['status'] = 'fail'
    else:
        msg['meta']['status'] = 'success'
    msg['data'] = doc.get_next_term()
    return msg

class CourseExecution(Document):

    @frappe.whitelist()
    def reexecute(self, term):
        # create a new document
        vals = dict()
        vals['doctype'] = 'Course Execution'
        vals['nb_of_assessments'] = self.nb_of_assessments
        vals['course'] = self.course
        vals['level'] = self.level
        vals['term'] = term
        doc = frappe.get_doc(vals)
        doc.insert()
        return doc.name

    @frappe.whitelist()
    def get_next_term(self):
        msg = dict()
        # get next term of current term (linked list)
        doc = frappe.get_doc('Term', self.term)
        msg["term"] = doc.get_next()
        return msg

