// Copyright (c) 2021, HFMTS and contributors
// For license information, please see license.txt

frappe.ui.form.on('Course Execution', {
    refresh(frm) {
        frm.add_custom_button('Reexecute', () => {
            reexecute(frm);
        });
    },
    onload: function(frm) {
        // order terms by "ordinal_number" field and archived...
        frm.set_query("term", function(_doc, _cdt, _cdn) {
            return {
                query: "uppsala.uppsala.doctype.term.term.term_query",
            }
        });
    }
});

function reexecute(frm) {
    frm.call('get_next_term').then(r => {
        if (r.message) {
            let msg = r.message;
            let d = new frappe.ui.Dialog({
                title: 'Select term',
                fields: [
                    {
                        label: 'Term',
                        fieldname: 'term',
                        fieldtype: 'Link',
                        options: "Term",
                        default: msg['term']
                    }      
                ],
                primary_action_label: 'Submit',
                primary_action(values) {
                    frm.call('reexecute', values).then(r => {
                        if (r.message) {
                            let msg = r.message;
                            frappe.set_route('course-execution', msg);
                        }
                    });
                    d.hide();
                }
            });

            d.show();
        }
    });
}
