// Copyright (c) 2021, HFMTS and contributors
// For license information, please see license.txt

frappe.ui.form.on('Attendance', {
 	refresh: function(frm) {
        frm.add_custom_button('Load participants', () => {
            update_participants(frm, "student_attendances");
        })
    },
    // frm passed as the first parameter
    course_execution(frm) {
        update_participants(frm, "student_attendances");
    },
});
