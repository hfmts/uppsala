# Copyright (c) 2021, HFMTS and contributors
# For license information, please see license.txt

# import frappe
from frappe.model.document import Document
from stdnum.ch import esr

class SchoolBillSettings(Document):


    def new_refnum(self, typenum=1):
        besr = self.setting_bill_besr_id
        # typenum 1 => student bills
        if typenum == 1:
            if self.setting_bill_runnum == None:
                self.setting_bill_runnum = 0
            # increment run number by one
            self.setting_bill_runnum += 1
            typenum = "1".zfill(11)
            runnum = str(self.setting_bill_runnum).zfill(9)
        # typenum 2 => generic bills
        elif typenum == 2:
            if self.setting_bill_runnum_2 == None:
                self.setting_bill_runnum_2 = 0
            # increment run number by one
            self.setting_bill_runnum_2 += 1
            typenum = "2".zfill(11)
            runnum = str(self.setting_bill_runnum_2).zfill(9)
        else:
            typenum = "3".zfill(11)
            runnum = "0".zfill(9)
        # save incremented run number
        self.save()

        basenum = besr + typenum + runnum
        refnum = basenum + esr.calc_check_digit(basenum)

        return esr.format(refnum)

