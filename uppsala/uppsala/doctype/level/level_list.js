frappe.listview_settings['Level'] = {

    hide_name_column: true, // hide the last column which shows the `name`
    onload(listview) {
        get_colors_level();
  },
  // format how a field value is shown
  formatters: {
      name_short(val) {
          return get_col_level(val);
      }
  }

}

