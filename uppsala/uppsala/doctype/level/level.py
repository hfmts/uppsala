# Copyright (c) 2021, HFMTS and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document


def get_levels_as_list() -> list[str]:
    docs = frappe.get_all('Level', fields=["name", "previous"])
    root = next((doc for doc in docs if doc["previous"] == None))
    levels:list[str] = [root["name"]]
    for i in range(len(docs)-1):
        next_lvl = next((doc for doc in docs if doc["previous"] == levels[i]))
        levels.append(next_lvl["name"])
    return levels


def get_passed_levels(level:str) -> list[str]:
    """
        get all passed and current level of the linked list of all levels
    """
    all_levels:list[str] = get_levels_as_list()
    lvl_idx = all_levels.index(level)
    levels = all_levels[:lvl_idx+1]
    return levels

class Level(Document):

    def get_next(self):
        docs = frappe.get_all('Level', filters={'previous': self.name})
        if len(docs) == 1:
            return docs[0]['name']
