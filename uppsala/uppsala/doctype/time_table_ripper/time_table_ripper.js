// Copyright (c) 2022, HFMTS and contributors
// For license information, please see license.txt

frappe.ui.form.on('Time Table Ripper', {
	// refresh: function(frm) {

	// }
    onload: function(frm) {
        console.log(`Rendering last results ${frm.doc.results}`);

        frm.get_field('html_status').$wrapper.html("");
        frm.get_field('html_stats').$wrapper.html("");
        frm.get_field('html_output').$wrapper.html("");

        if (frm.doc.results !== undefined) {
            let msg = JSON.parse(frm.doc.results);
            display_results(frm, msg);
        }    
    },
	process_time_table(frm) {
		process_time_table(frm);
	}
});

function display_results(frm, msg) {
    // console.log(msg["data"]["output"]);
    let success = false;
    let html_status = "";
    if (msg.meta.code == RPCResponseCode.SUCCESS) {
        success = true;
        html_status = `
        <div class="alert alert-success" role="alert">
          Successfully finished time table processing! <br>
          Last executed: ${msg.data.time} <br>
        </div>`;
    }
    else {
        html_status = `
        <div class="alert alert-danger" role="alert">
          Finished time table processing with an error: ${msg.meta.msg} <br>
        </div>`;
    }   
    frm.get_field('html_status').$wrapper.html(html_status);

    if (!success) { return; }
    let rows_stat = '';
    for (let i in msg.data.stats) {
        let stat = msg.data.stats[i];
        let link_blocks = `<a href="/files/${frm.doc.name}/blocks_${stat.course_name}.csv"><i class="fa fa-download"></i></a>`
        let link_attendance = `<a href="/files/${frm.doc.name}/attendance_${stat.course_name}.pdf"><i class="fa fa-download"></i></a>`
        if (stat.course_description == "missing config") {
            link_blocks = "";
            link_attendance = "";
        }
        if (stat.course_name == "Total") {
            link_attendance = "";
        }
        rows_stat +=
        `<div class="grid-row">
        <div class="data-row row">
            <div class="col grid-static-col col-xs-2">
                ${stat.course_name}
            </div>
            <div class="col grid-static-col col-xs-4">
                ${stat.course_description}
            </div>
            <div class="col grid-static-col col-xs-1">
                ${stat.count_lecs}
            </div>
            <div class="col grid-static-col col-xs-1">
                ${stat.count_blocks}
            </div>
            <div class="col grid-static-col col-xs-2">
                ${link_blocks}
            </div>
            <div class="col grid-static-col col-xs-2">
                ${link_attendance}
            </div>
        </div>
        </div>`;
    }

    let html_stats = `
        <div class="form-grid">
            <div class="grid-heading-row">
            <div class="grid-row">
            <div class="data-row row">
                <div class="col grid-static-col col-xs-2">
                    <div class="static-area ellipsis">${__('Course')}</div>
                </div>
                <div class="col grid-static-col col-xs-4">
                    <div class="static-area ellipsis">${__('Description')}</div>
                </div>
                <div class="col grid-static-col col-xs-1">
                    <div class="static-area ellipsis">${__('# Lectures')}</div>
                </div>
                <div class="col grid-static-col col-xs-1">
                    <div class="static-area ellipsis">${__('# Blocks')}</div>
                </div>
                <div class="col grid-static-col col-xs-2">
                    <div class="static-area ellipsis">${__('Moodle Sessions')}</div>
                </div>
                <div class="col grid-static-col col-xs-2">
                    <div class="static-area ellipsis">${__('Attendance Forms')}</div>
                </div>
            </div>
            </div>
            </div>
            ${rows_stat}
        </div>`;

    frm.get_field('html_stats').$wrapper.html(html_stats);

    let getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host;

    let html_output = '';
    for (let i in msg.data.output) {
        html_output += msg.data.output[i] + "<br>";
    }
    frm.get_field('html_output').$wrapper.html(html_output);
}

function process_time_table(frm) {

    if (frm.is_dirty()) {
        frm.save();
    }

    let infomsg = __("Started time table processing, please wait!");
    frappe.show_alert({message: infomsg, indicator: "blue"}, 10);
    frm.call('process_time_table')
    .then((r) => {
        let msg = r.message;
        console.log(`Process time table returned ${msg.meta.msg}`);

        display_results(frm, msg);

    });

}

