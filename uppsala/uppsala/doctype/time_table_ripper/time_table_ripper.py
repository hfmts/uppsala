# Copyright (c) 2022, HFMTS and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
import os
from pathlib import Path
import yaml
import json
import subprocess
from datetime import datetime
from shutil import copy, rmtree
import uppsala.core.utils
from uppsala.core.rpc import RPCResponse, RPCResponseCode



frappe.utils.logger.set_log_level("DEBUG")
logger = frappe.logger("uppsala")

class TimeTableRipper(Document):


    def after_delete(self):
        path_files = self.get_files_path()
        logger.info(f"Deleting files in public folder {path_files}")
        try:
            rmtree(path_files)
        except Exception as err:
            logger.error(err)
    
    @frappe.whitelist()
    def process_time_table(self) -> RPCResponse:
        data = dict() 
        data['time'] = str(datetime.now())

        logger.info(f"Processing time table {self.time_table}")

        try:
            config = self.construct_config()
        except Exception as err:
            msg = f"Could not construct config. {err}"
            logger.error(msg)
            return {"meta": { "code": RPCResponseCode.FAIL, "msg": msg}}
        
        # write config to disk
        path_root = uppsala.core.utils.get_root_path()
        path_files = uppsala.core.utils.get_files_path(self.name)
        path_output = path_root/path_files
        path_cfg = path_output/"config.yaml"
        path_ttr = Path("/workspace/frappe-bench/apps/uppsala/uppsala/uppsala/doctype/time_table_ripper/ttr")
        path_templates = path_ttr/"templates"
        path_time_table = path_root/self.time_table[1:] # remove leading front slash to avoid abspath
        path_time_table_copy = path_output/"timetable.xlsx"
        path_results = path_cfg/"results.json"

        # copy excel into output folder (to allow copying it to test instance)
        path_output.mkdir(parents=True, exist_ok=True)
        copy(path_time_table, path_time_table_copy)

        logger.info(f"Writing config file to {path_cfg}")
        with open(path_cfg, "w") as f:
            yaml.dump(config, f)
        self.config_file = yaml.dump(config)
        
        copy(path_time_table, path_time_table_copy)

        # call ttr
        cmd = [str(path_ttr/"ttr")]
        if self.verbosity == "Debug":
            cmd += ['-v']
        if self.skip_pdf:
            cmd += ['--skip-pdf']
        cmd += ["--config", str(path_cfg), "--output", str(path_output), "--templates", str(path_templates), str(path_time_table_copy)]

        logger.info(f"Running command {cmd}")
        ret = subprocess.run(cmd, capture_output=True, env={"RUST_LOG": "INFO"})
        logger.info(f"ttr returned code {ret.returncode}")
        lines = ret.stderr.decode("utf8").split("\n") 
        lines_mod = list()
        for line in lines:
            line_mod = line.replace("INFO", '<mark style="color: green; background-color: white;">INFO</mark>')
            line_mod = line_mod.replace("DEBUG", '<mark style="color: blue; background-color: white;">DEBUG</mark>')
            line_mod = line_mod.replace("ERROR", '<mark style="color: red; background-color: white;">ERROR</mark>')
            line_mod = line_mod.replace("WARN", '<mark style="color: orange; background-color: white;">WARN</mark>')
            lines_mod.append(line_mod)
        data["output"] = [ f"{cmd}" ]
        data["output"] += lines_mod
        data["config"] = config

        if ret.returncode != 0:
            return {"meta": { "code": RPCResponseCode.FAIL, "msg": f"ttr returned code {ret.returncode}. View log for details."} , "data": data }
        else:
            # construct return message
            with open(path_output/"stats.json") as file:
                data["stats"] = json.load(file)
        self.stats = json.dumps(data["stats"])
        self.last_executed = str(datetime.now())
        # save message for next display
        msg = {"meta": { "code": RPCResponseCode.SUCCESS, "msg": "" }, "data": data }
        self.results = json.dumps(msg)
        self.save()
        return msg


    def construct_config(self):
        config = dict()
        # get student list for class and term and level
        filters = { "term": self.term, "school_class": self.school_class, "level": self.level }
        enrollments = frappe.get_all("Enrollment", filters=filters, fields=["name", "student_first_name", "student_last_name", "level"], order_by="student_last_name")

        if len(enrollments) == 0:
            raise Exception(f'No enrollments found for school class {self.school_class} and level {self.level} in term {self.term}')

        # get all course executions for term and level
        course_executions = list()
        filters = {"term": self.term, "level": self.level}
        course_executions = frappe.get_all("Course Execution", filters=filters, fields=["name", "course_description", "course"])
    

        config["term"] = self.term
        config["school_class"] = self.school_class
        config["level"] = self.level
        config["students"] = list()
        for enrollment in enrollments:
            config["students"].append([enrollment["student_first_name"], enrollment["student_last_name"]])

        config["courses"] = list()

        # convert Class19 to Klasse 2019
        class_id = self.school_class[5:7]

        # convert course execution to moodle course names
        for course_execution in course_executions:
            logger.info(f"Processing {course_execution}")
            course = frappe.get_doc("Course", course_execution["course"])
            # construct moodle course name
            moodle_levels = {"Sem1": "1S", "Sem2": "2S", "Sem3": "3S", "Sem4": "4S", "Sem5": "5S", "Sem6": "6S", "Sem7": "7S"}
            moodle_course = f"{course.name_short} {moodle_levels[self.level]} (Klasse 20{class_id})"

            course_name = course.name_short
            # handle special case english, where there are A1, A2, B1 courses,
            # however in the timetable, they are all displayed as "TE"
            if course.name_short.startswith("TE"):
                course_name = "TE"

            config["courses"].append({ "name_short": course_name, "description": course.description, "name_moodle": moodle_course})

        return config
