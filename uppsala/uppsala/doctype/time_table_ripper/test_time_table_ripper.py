# Copyright (c) 2022, HFMTS and Contributors
# See license.txt

import frappe
import unittest

class TestTimeTableRipper(unittest.TestCase):
    
    def test_process_time_table(self):
        ttr = frappe.get_doc("Time Table Ripper", "TTR-0001")
        msg = ttr.process_time_table()
        print(msg)


    def test_construct_config(self):
        ttr = frappe.get_doc("Time Table Ripper", "TTR-0001")
        config = ttr.construct_config()
        print(config)