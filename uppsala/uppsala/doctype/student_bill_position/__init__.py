

def execute(filters=None):
    columns, data = [], []
    # set filters for debuging with bench execute uppsala.uppsala.report.grade_list.grade_list.execute
    # filters = {'school_class': 'Class21', 'term': 'WS21/22', 'level': 'Sem2'}
    # logger.debug(filters)
    # check if school_class and term or level filter are set
    if not filters:
        return None
    if not ("school_class" in filters and "term" in filters):
        return None
    # get all enrollments of given term and school class
    dict_enrollments = frappe.get_all("Enrollment", filters=filters, fields=["name", "student", "level"])


    level = dict_enrollments[0]["level"]
    for dict_enrollment in dict_enrollments:
        if dict_enrollment["level"] != level:
            msg = f"School class {filters['school_class']} in term {filters['term']} contains different levels ({dict_enrollments[0]['name']}:{dict_enrollments[0]['level']};{dict_enrollment['name']}:{dict_enrollment['level']})"
            frappe.msgprint(msg="", title="Error")
    # print(f"dict_enrollments={dict_enrollments}")
    # find all course executions that students are enrolled in
    name_course_executions = list()
    for dict_enrollment in dict_enrollments:
        doc_enrollment = frappe.get_doc("Enrollment", dict_enrollment["name"])
        for course_enrollment in doc_enrollment.course_enrollments:
            if course_enrollment.course_execution not in name_course_executions:
                name_course_executions.append(course_enrollment.course_execution)
    # print(name_course_executions)
    # get the short names of the courses
    course_execution_infos = dict()
    for name_course_execution in name_course_executions:
        doc_course_execution = frappe.get_doc("Course Execution", name_course_execution)
        name_course = doc_course_execution.course
        doc_course = frappe.get_doc("Course", name_course)
        info = {"name_short": doc_course.name_short, "labels_assessments": {}, "labels_avg_fin": False}
        course_execution_infos[name_course_execution] = info
    # print(columns)
    # go through all enrollments and get all assessments
    columns.append({ "fieldname": "first_name", "label": "First Name", "fieldtype": "Data"})
    columns.append({ "fieldname": "last_name", "label": "Last Name", "fieldtype": "Data"})
    for dict_enrollment in dict_enrollments:
        doc_enrollment = frappe.get_doc("Enrollment", dict_enrollment["name"])
        row = dict()
        row["first_name"] = doc_enrollment.student_first_name
        row["last_name"] = doc_enrollment.student_last_name
        dict_scores = doc_enrollment.get_course_scores()
        for dict_course_execution in dict_scores["data"]:
            # logger.debug(dict_course_execution)
            info = course_execution_infos[dict_course_execution["course_execution"]]
            assessments = dict_course_execution["assessments"]

            for assessment in assessments:
                # construct column name
                n = len(info["labels_assessments"])
                column_name = f'{info["name_short"]} {n+1}'
                # create new column labels
                if assessment["name"] not in info["labels_assessments"]:
                    # save column label for next enrollment
                    info["labels_assessments"][assessment["name"]] = column_name
                    # add column label to columns
                    columns.append({ "fieldname": column_name, "label": column_name, "fieldtype": "Number"})
                else:
                    column_name = info["labels_assessments"][assessment["name"]]
                # add grade
                row[column_name] = assessment["grade_value"]
            # construct column name
            column_name_avg = f'{info["name_short"]} AVG'
            column_name_fin = f'{info["name_short"]} FIN'
            column_name_att = f'{info["name_short"]} ATT'
            # create new column labels
            if not info["labels_avg_fin"]:
                columns.append({ "fieldname": column_name_avg, "label": column_name_avg, "fieldtype": "Number"})
                columns.append({ "fieldname": column_name_fin, "label": column_name_fin, "fieldtype": "Number"})
                columns.append({ "fieldname": column_name_att, "label": column_name_att, "fieldtype": "Text"})
                # set labels_avg_fin to true to avoid adding for other enrollments
                info["labels_avg_fin"] = True
            # add grades
            if "grade_final_raw" in dict_course_execution:
                row[column_name_avg] = dict_course_execution["grade_final_raw"]
            row[column_name_fin] = dict_course_execution["grade_final"]
            row[column_name_att] = dict_course_execution["attendance"]["value"]
        
        data.append(row)

    # print(data)
     
    return columns, data
