# Copyright (c) 2021, HFMTS and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
import math
from uppsala.core.rpc import RPCResponse, RPCResponseCode

import json
frappe.utils.logger.set_log_level("DEBUG")
logger = frappe.logger("uppsala")

from enum import Enum

class BulkCourseEnrollmentAction(int, Enum):
    REPLACE = 0
    APPEND = 1


@frappe.whitelist()
def bulk_course_enrollments(enrollments, course_executions, action:BulkCourseEnrollmentAction) -> RPCResponse:

    enrollments = json.loads(enrollments)
    course_executions = json.loads(course_executions)
    
    for enrollment in enrollments:
        logger.info(f"Performing {BulkCourseEnrollmentAction(int(action)).name} action for course executions on {enrollment}")
        if int(action) == BulkCourseEnrollmentAction.REPLACE:
            logger.debug(f"Deleting existing course enrollements for {enrollment}")
            try:
                frappe.db.delete("Course Enrollment", {"parent": enrollment})
                frappe.db.commit()
            except Exception as err:
                return { "meta": { "code": RPCResponseCode.FAIL}, "msg": f"Failed to delete course enrollments for {enrollment}: {err}"}
                                    
        doc = frappe.get_doc("Enrollment", enrollment)
        for course_execution in course_executions:
            try:
                doc.append("course_enrollments", {"course_execution": course_execution})
            except Exception as err:
                return { "meta": { "code": RPCResponseCode.FAIL}, "msg": f"Failed to create course enrollments for {enrollment} and {course_execution}: {err}"}

        doc.save()
    return { "meta": { "code": RPCResponseCode.SUCCESS }}
            
         
@frappe.whitelist()
def bulk_level_up(items, term, level, load_course_executions) -> RPCResponse:
    """level up items"""
    items = json.loads(items)
    
    data = list()

    for item in items:
        doc = frappe.get_doc('Enrollment', item['name'])
        name = doc.level_up(term, level, load_course_executions)
        data.append(name)
    if len(items) >= len(data):
        return { "meta": { "code": RPCResponseCode.SUCCESS}, "data": data }
    return { "meta": { "code": RPCResponseCode.FAIL, "msg": "Failed to level up some items"}}

@frappe.whitelist()
def get_level_up_values(docname):
    msg = dict()
    msg['data'] = list()
    msg['meta'] = dict()
    doc = frappe.get_doc('Enrollment', docname)
    if doc == None:
        msg['meta']['status'] = 'fail'
    else:
        msg['meta']['status'] = 'success'
    msg['data'] = doc.get_level_up_values()
    return msg

class Enrollment(Document):

    @frappe.whitelist()
    def get_level_up_values(self):
        msg = dict()
        try:
            doc = frappe.get_doc('Level', self.level)
            msg["level"] = doc.get_next()
        except ValueError as err:
            msg["level"] = ""
            pass
        # get next term of current term (linked list)
        doc = frappe.get_doc('Term', self.term)
        msg["term"] = doc.get_next()
        return msg

    @frappe.whitelist()
    def level_up(self, term, level, load_course_executions=True):
        # create a new document
        vals = dict()
        vals['doctype'] = 'Enrollment'
        vals['student'] = self.student
        vals['school_class'] = self.school_class
        vals['term'] = term
        vals['level'] = level
        vals['student_first_name'] = self.student_first_name
        vals['student_last_name'] = self.student_last_name
        if load_course_executions:
            vals['course_enrollments'] = list()
            msg = self.get_course_executions(term, level)
            for course_execution in msg["data"]:
                course_enrollment = dict()
                course_enrollment['course_execution_course_description'] = course_execution["course_description"]
                course_enrollment['course_execution'] = course_execution["name"]
                vals['course_enrollments'].append(course_enrollment)       
        doc = frappe.get_doc(vals)
        doc.insert()
        return doc.name

    def get_course_execution(self, entries, course_execution):
        """
            assuming entires is a list of dicts with a key course_execution, return all dicts
        """       
        ret = list()
        for entry in entries:
            if entry["course_execution"] == course_execution:
                ret.append(entry)     
        return ret

    @frappe.whitelist()
    def get_course_executions(self, term, level):
        msg = dict()
        msg["meta"] = dict()
        msg["data"] = list()

        # get enrollment
        msg["meta"]["Student"] = self.student
        msg["meta"]["Term"] = term
        msg["meta"]["Level"] = level
        filters = {"term": term}
        if self.level != "":
            filters["level"] = level
        course_executions = frappe.get_all("Course Execution", filters=filters, fields=["name", "course_description"])
        for course_execution in course_executions:
            msg["data"].append(course_execution)
        return msg


    @frappe.whitelist()
    def get_course_scores(self, only_final=False):
        msg = dict()
        msg["meta"] = dict()
        msg["data"] = list()

        enrollment = self

        # get enrollment
        msg["meta"]["Student"] = enrollment.student
        msg["meta"]["Term"] = enrollment.term
        if not hasattr(enrollment, "course_enrollments"):
            return msg

        scores = list()
        logger.info(f"get_course_scores: Processing {enrollment.student}")
        for course_enrollment in enrollment.course_enrollments:
            score = dict()
            course_execution = frappe.get_doc("Course Execution", course_enrollment.course_execution)
            logger.debug(f"Processing {course_execution.course_description} ({course_enrollment.course_execution})")
            score["course_description"] = course_execution.course_description
            score["course_execution"] = { "name": course_execution.name, "nb_of_assessments": course_execution.nb_of_assessments }

            # handling attendance
            attendance_summary = { "name": "", "value": ""}
            attendances = frappe.get_all("Attendance",  filters={"course_execution": course_enrollment.course_execution}, fields = ["name"])
            attendance_name = None
            if len(attendances) == 0:
                logger.warning(f"No attendance in course, skipping...")
            elif len(attendances) > 1:
                logger.warning(f"More than one attendance in course {course_execution.name}, picking first...")
                attendance_name = attendances[0]["name"]
            else:
                attendance_name = attendances[0]["name"]
            if attendance_name is not None:
                attendance_value = ""
                attendance_values = frappe.get_all("Student Attendance", filters={"parent": attendance_name, "student": enrollment.student}, fields=["name", "attendance_value"])
                if len(attendance_values) == 0:
                    logger.warning(f"No attendance for student, skipping...")
                elif len(attendance_values) > 1:
                    logger.warning(f"More than one attendance values for student, picking first...")
                    attendance_value = attendance_values[0]["attendance_value"]
                else:
                    attendance_value = attendance_values[0]["attendance_value"]
                attendance_summary["name"] = attendance_name
                attendance_summary["value"] = attendance_value
            score["attendance"] = attendance_summary

            # handling grades
            assessment_summary = { }
            assessments = frappe.get_all("Assessment",  filters={"course_execution": course_enrollment.course_execution}, fields = ["name", "weight", "type"])
            has_type_text = False
            has_type_number = False
            has_bad_number = False
            grade_values = list()
            weights = list()
            score["assessments"] = list()
            for assessment in assessments:
                # keep track of assesment types
                if assessment["type"] == "Number":
                    has_type_number = True
                elif assessment["type"] == "Text":
                    has_type_text = True

                grade_value = "?"
                grades = frappe.get_all("Student Assessment", filters={"parent": assessment["name"], "student": enrollment.student}, fields=["name", "grade_value"])
                if len(grades) == 0:
                    logger.warning(f"No grade value for student, skipping...")
                elif len(grades) > 1:
                    logger.warning(f"More than one grade values for student, picking first...")
                else:
                    if assessment["type"] == "Number":
                        try:
                            grade_value = float(grades[0]["grade_value"])
                            # multiply grade value with weight 
                            grade_values.append(assessment["weight"]*grade_value)
                            weights.append(assessment["weight"])
                        except ValueError as err:
                            has_bad_number = False
                            grade_value = f"? {grades[0]['grade_value']} ?"
                    elif assessment["type"] == "Text":
                        grade_value = grades[0]["grade_value"]
                        grade_values.append(grade_value)
                assessment["grade_value"] = grade_value
                score["assessments"].append(assessment)

            # calculate final grade
            if has_type_number and has_type_text:
                score["course_assessment_type"] = "Mixed"
                score["grade_final_raw"] = "-"
                score["grade_final"] = "mixed types"
            elif has_type_text:
                score["course_assessment_type"] = "Text"
                if len(grade_values) == 0:
                    score["grade_final"] = "-"
                elif len(grade_values) > 1:
                    score["grade_final"] = "?"
                else:
                    score["grade_final"] = grade_values[0]
            elif has_bad_number:
                score["course_assessment_type"] = "Number"
                score["grade_final_raw"] = "-"
                score["grade_final"] = "?"
            elif has_type_number:
                score["course_assessment_type"] = "Number"
                if len(grade_values) == 0:
                    score["grade_final_raw"] = "-"
                    score["grade_final"] = "-"
                else:
                    # calculate average
                    average = round(sum(grade_values)/sum(weights),2)

                    # calculate rounded average based on user setting
                    average_rounded = 0
                    if course_execution.round_setting == "none":
                        average_rounded = average
                    else:
                        # create bins: [[1.0, 0.75, 1.25],[1.5, 1.25, 1.75],...,[5.5, 5.25, 5.75],[6.0, 5.75, 6.25]]
                        grade_ranges = [[x/2, x/2-0.25,x/2+0.25] for x in range(2,2*7-1)]
                        for gr in grade_ranges:
                            if average >= gr[1] and average < gr[2]:
                                average_rounded = gr[0]
                                break
                    score["grade_final_raw"] = average
                    score["grade_final"] = average_rounded
            else:
                score["course_assessment_type"] = ""
                score["grade_final_raw"] = ""
                score["grade_final"] = ""
            scores.append(score)
        
        # sort alphapticaly
        scores.sort(key=lambda x: x["course_description"], reverse=False)
        msg["data"] = scores
        return msg

    
 
