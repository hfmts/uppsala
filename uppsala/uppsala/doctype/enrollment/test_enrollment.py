# Copyright (c) 2021, HFMTS and Contributors
# See license.txt

import frappe
import unittest

class Testenrollment(unittest.TestCase):
    
    def setUp(self):
        pass

    def test_get_course_scores(self):
        doc = frappe.get_doc("Enrollment", "ENR-000155")
        scores = doc.get_course_scores_new()
        print(f"Scores new for student {doc.student}")
        for score in scores["data"]:
            print(score)