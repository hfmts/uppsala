// Copyright (c) 2021, HFMTS and contributors
// For license information, please see license.txt

frappe.ui.form.on('Enrollment', {
    refresh(frm) {
        frm.add_custom_button('Level up', () => {
            level_up(frm);
        });
    },
    load_course_executions(frm) {
        update_course_executions(frm, frm.docname);
    },
    onload(frm) {
        update_assessments(frm, frm.docname);

        // order terms by "ordinal_number" field and archived...
        frm.set_query("term", function(_doc, _cdt, _cdn) {
            return {
                query: "uppsala.uppsala.doctype.term.term.term_query",
            }
        });
    },
    load_assessments(frm) {
        update_assessments(frm, frm.docname);
    }
});

function level_up(frm) {
    frm.call('get_level_up_values').then(r => {
        if (r.message) {
            let msg = r.message;
            let d = new frappe.ui.Dialog({
                title: 'Select term and level',
                fields: [
                    {
                        label: 'Term',
                        fieldname: 'term',
                        fieldtype: 'Link',
                        options: "Term",
                        default: msg['term']
                    },
                    {
                        label: 'Level',
                        fieldname: 'level',
                        fieldtype: 'Data',
                        default: msg['level']
                    }       
                ],
                primary_action_label: 'Submit',
                primary_action(values) {
                    frm.call('level_up', values).then(r => {
                        if (r.message) {
                            let msg = r.message;
                            frappe.set_route('enrollment', msg);
                        }
                    });
                    d.hide();
                }
            });

            d.show();
        }
    });
}

function update_course_executions(frm) {
    console.log(frm.doc.term);
    console.log(frm.doc.level);
    const vals =  { term: frm.doc.term, level: frm.doc.level};
    frm.call('get_course_executions', vals).then(r => {
        if (r.message) {
            let msg = r.message;
            let rows = '';
            // remove all entries
            frm.clear_table('course_enrollments');
            // go over all found course executions
            for (let i in msg.data) {
                let row = frm.add_child('course_enrollments', 
                    {   course_execution: msg.data[i].name,
                        course_execution_course_description: msg.data[i].course_description
                    }
                );
            }
            frm.refresh_field('course_enrollments');

        }
    });
}


