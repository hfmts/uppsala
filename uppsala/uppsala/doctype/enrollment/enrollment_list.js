frappe.listview_settings['Enrollment'] = {

    hide_name_column: true, // hide the last column which shows the `name`
    onload(listview) {

        if (listview.page.fields_dict.term) {
            // order terms by "ordinal_number" field and archived...
            listview.page.fields_dict.term.get_query = function(_doc, _cdt, _cdn) {
                return {
                    query: "uppsala.uppsala.doctype.term.term.term_query",
                }
            };
        }

        listview.page.add_action_item(__('Level up'), () => level_up());
        listview.page.add_action_item(__('Create Student Bill'), () => create_student_bill_start());
        listview.page.add_action_item(__('Create Grade Report'), () => create_grade_report_start());
        listview.page.add_action_item(__('Bulk Course Enrollments'), () => bulk_course_enrollments_start());
        get_colors_level();
  },
  // format how a field value is shown
  formatters: {
      level(val) {
          return get_col_level(val);
     }
  }

}


/**
 * Bill creation: Create a student bill record for each selected enrollment. 
 */

function create_student_bill_start() {
    console.log('Create Student Bill');
    let listview = frappe.get_list_view('Enrollment');
    let items = listview.get_checked_items();
    console.log(items);
    // inform about duplicates
    frappe
    .call({
        method: 'uppsala.core.utils.check_duplicates',
        freeze: true,
        args: {
            items: items,
            doctype: "Student Bill",
            field: "enrollment"
        }
    })
    .then((r) => {
        let msg = r.message;
        console.log(msg);
        question = __("Create {0} Student Bill?", [items.length]) + "<br><br>";
        if (msg.data.length > 0) {
          question += __("Student Bills for some Enrollments already exist:") + "<br>";
          question += `[${msg.data.join(',')}]`;
        }

        frappe.confirm(question, ()=> create_student_bill_finalize(items), () => {});

    });

}

function create_student_bill_finalize(items) {
    frappe
    .call({
        // method: 'uppsala.uppsala.doctype.enrollment.enrollment.get_level_up_values',
        method: 'uppsala.uppsala.doctype.student_bill.student_bill.bulk_create_student_bills',
        freeze: true,
        args: {
            enrollments: items
        }
    })
    .then((r) => {
        let msg = r.message;
        console.log(msg);
        if (msg.meta.code == RPCResponseCode.SUCCESS) {
            let infomsg = __("Successfully created {0} Student Bill records!", [msg.data.length]);
            frappe.show_alert({message: infomsg, indicator: "green"}, 10);
        }
        else {
            let infomsg = __("Error creating Student Bill records, only created {0} Student Bill records!", [msg.data.length]);
            frappe.show_alert({message: infomsg, indicator: "red"}, 20);
        }

        // finally change to student bill view
        var csv = msg.data.join(',');
        console.log(`Moving to student_bill/list with filter showing only ${csv}`);
        frappe.route_options = { "name": ["in", csv] };
        frappe.set_route(["List", "Student Bill", "List"]);
    });
}


/**
 * Grade report creation: Create a grade report record for each selected enrollment. 
 */

function create_grade_report_start() {
    console.log('Create Grade Report');
    let listview = frappe.get_list_view('Enrollment');
    let items = listview.get_checked_items();
    console.log(items);
    // inform about duplicates
    frappe
    .call({
        method: 'uppsala.core.utils.check_duplicates',
        freeze: true,
        args: {
            items: items,
            doctype: "Grade Report",
            field: "enrollment"
        }
    })
    .then((r) => {
        let msg = r.message;
        console.log(msg);
        question = __("Create {0} Grade Report?", [items.length]) + "<br><br>";
        if (msg.data.length > 0) {
          question += __("Grade Reports for some Enrollments already exist:") + "<br>";
          question += `[${msg.data.join(',')}]`;
        }

        frappe.confirm(question, ()=> create_grade_report_finalize(items), () => {});

    });
}

function create_grade_report_finalize(items) {
    frappe
    .call({
        method: 'uppsala.uppsala.doctype.grade_report.grade_report.bulk_create_grade_reports',
        freeze: true,
        args: {
            report_type: "Semester",
            enrollments: items
        }
    })
    .then((r) => {
        let msg = r.message;
        console.log(msg);
        if (msg.meta.code == RPCResponseCode.SUCCESS) {
            let infomsg = __("Successfully created {0} Grade Report records!", [msg.data.length]);
            frappe.show_alert({message: infomsg, indicator: "green"}, 10);
        }
        else {
            let infomsg = __("Error creating Grade Report records, only created {0} Grade Report records!", [msg.data.length]);
            frappe.show_alert({message: infomsg, indicator: "red"}, 20);
        }

      // finally change to grade report view
      var csv = msg.data.join(',');
      console.log(`Moving to grade_report/list with filter showing only ${csv}`);
      frappe.route_options = { "name": ["in", csv] };
      frappe.set_route(["List", "Grade Report", "List"]);
    });
}

/**
 * Level up: Create a new enrollment based on previous enrollment and only change term and level.
 */
function level_up() {
    console.log('Level up');
    // get selected items
    let listview = frappe.get_list_view('Enrollment');
    let items = listview.get_checked_items();
    let term_dft = items[0].term;
    let level_dft = items[0].level;
    let school_class_dft = items[0].school_class;
    // check if items have the same values for term, level and school class
    let sane = true;
    let msg = "";
    for (i in items) {
        console.log(items[i]);
        if (term_dft != items[i].term) {
            msg = "Selected items have different terms: " + term_dft + " and " + items[i].term;
            sane = false;
        }
        if (level_dft != items[i].level) {
            msg = "Selected items have different levels: " + level_dft + " and " + items[i].level;
            sane = false;
        }
        if (school_class_dft != items[i].school_class) {
            msg = "Selected items have different school class: " + school_class_dft + " and " + items[i].school_class;
            sane = false;
        }
    }
    if (!sane) {
        // with options
        frappe.msgprint({
            title: __('Item selection error'),
            indicator: 'red',
            message: msg
        });
        return;
    }
    // get next term and level
    frappe
    .call({
        method: 'uppsala.uppsala.doctype.enrollment.enrollment.get_level_up_values',
        freeze: true,
        args: {
            docname: items[0].name  // assuming all selected items have same term and level
        }
    })
    .then((r) => {
        let msg = r.message;
        let term_new = msg['data']['term'];
        let level_new = msg['data']['level'];
        // finally present dialog
        open_levelup_dialog(items, term_new, level_new, school_class_dft);
        listview.refresh();
    });
}

function open_levelup_dialog(items, term_dft, level_dft, school_class_dft) {
    let d = new frappe.ui.Dialog({
        title: 'Select enrollment parameters.',
        fields: [
            {
                label: 'Term',
                fieldname: 'term',
                fieldtype: 'Link',
                options: "Term",
                default: term_dft
            },
            {
                label: 'Level',
                fieldname: 'level',
                fieldtype: 'Link',
                options: "Level",
                default: level_dft
            },
            {
                label: "Autoload course executions",
                fieldname: "autoload_course_executions",
                fieldtype: "Check",
                default: 1
            }
        ],
        primary_action_label: 'Submit',
        primary_action(values) {
            bulk_level_up(items, values.term, values.level, values.autoload_course_executions);
            d.hide();
        }
    });

    d.show();
}

function bulk_level_up(items, term, level, load_course_executions) {
    let done = null;
    frappe
    .call({
        method: 'uppsala.uppsala.doctype.enrollment.enrollment.bulk_level_up',
        freeze: true,
        args: {
            items: items,
            term: term,
            level: level,
            load_course_executions: load_course_executions
        }
    })
    .then((r) => {
        let msg = r.message;
        console.log(msg);

        if (msg.meta.code == RPCResponseCode.SUCCESS) {
            let infomsg = __("Successfully leveled up {0} Enrollment records!", [msg.data.length]);
            frappe.show_alert({message: infomsg, indicator: "green"}, 10);
            frappe.utils.play_sound("submit");
        }
        else {
            let infomsg = __("Error leveling up some Enrollments. {0} Enrollments leveled up successfully!", [msg.data.length]);
            frappe.show_alert({message: infomsg, indicator: "red"}, 20);
            frappe.utils.play_sound("delete");
        }
    });
}

/**
 * Bulk course enrollments: Adjust course enrollments. 
 */

function bulk_course_enrollments_start() {
    console.log('Start bulk course enrollments');
    let listview = frappe.get_list_view('Enrollment');
    let items = listview.get_checked_items();
    console.log(items);
    // check if all enrollments are of same level and term
    let level = items[0].level;
    let term = items[0].term;
    let enrollments = new Array();
    for (let i in items) {
        if (items[i].level != level || items[i].term != term) {
            frappe.throw("Selected Enrollments not in same level and term. Please correct that!");
            return;
        }
        enrollments.push(items[i].name)
    }
    console.log(enrollments)
    
    let ms = new frappe.ui.form.MultiSelectDialog({
        doctype: "Course Execution",
        target: this.cur_frm,
        setters: {
            course_description: null,
            term: term,
            level: level,
        },
        add_filters_group: 1,
        date_field: "transaction_date",
        primary_action_label: __("Use selected"),
        get_query() {
            return {
                filters: { 
                    term: term,
                    level: level,
                }
            }
        },
        action(selections) {
            console.log(selections);
            bulk_course_enrollments_confirm(enrollments, selections);
            ms.dialog.hide();
        }
    });

}

function bulk_course_enrollments_confirm(enrollments, course_executions) {
    // with primary action

    let d = new frappe.ui.Dialog({ 
        title: __("Bulk Course Enrollments"),
        fields: [{
            label: null,
            fieldtype: "HTML",
            options: __("Do you want to replace or append the {0} course enrollments to {1}  enrollments?", [course_executions.length, enrollments.length])
        }],
        primary_action_label: __("Replace"),
        primary_action: () => {
            bulk_course_enrollements_finalize(enrollments, course_executions, BulkCourseEnrollmentAction.REPLACE);
            d.hide();
        },
        secondary_action_label: __("Append"),
        secondary_action: () => {
            bulk_course_enrollements_finalize(enrollments, course_executions, BulkCourseEnrollmentAction.APPEND);
            d.hide();
        }
    });
    d.show();
}

const BulkCourseEnrollmentAction = {
  REPLACE:0,
  APPEND:1
};


async function bulk_course_enrollements_finalize(enrollments, course_executions, action) {

    console.log(`Rewriting course enrollements for ${enrollments} to ${course_executions}`);

    let ret = await frappe
    .call({
        method: 'uppsala.uppsala.doctype.enrollment.enrollment.bulk_course_enrollments',
        freeze: true,
        args: {
            enrollments: enrollments,
            course_executions: course_executions,
            action: action
        }
    });
    let msg = ret.message;
    console.log(msg);
 
    if (msg.meta.code == RPCResponseCode.SUCCESS) {

        let infomsg = "";
        switch (action) { 
             case BulkCourseEnrollmentAction.REPLACE: infomsg = __("Successfully replaced {0} course executions in {1} Enrollment records!", [course_executions.length, enrollments.length ]); break;
             case BulkCourseEnrollmentAction.APPEND: infomsg = __("Successfully appended {0} course executions in {1} Enrollment records!", [course_executions.length, enrollments.length ]); break;
            default: break;
        }
        frappe.show_alert({message: infomsg, indicator: "green"}, 10);
        frappe.utils.play_sound("submit");

    }
    else {
        let infomsg = __("Error creating course enrollments: {}!", [msg.meta.msg]);
        frappe.throw(infomsg);
        frappe.utils.play_sound("delete");
    }}
