# Copyright (c) 2023, HFMTS and contributors
# For license information, please see license.txt

import frappe


def execute(filters=None):
    columns, data = [], []

    # if not filters:
    #     return None
    # if not ("school_class" in filters and "term" in filters):
    #     return None
    # get all enrollments of given term and school class
    columns.append({ "fieldname": "date", "label": "Date", "fieldtype": "Date"})
    columns.append({ "fieldname": "invoice_nb", "label": "Invoice Nb.", "fieldtype": "Data"})
    columns.append({ "fieldname": "debtor", "label": "Debtor", "fieldtype": "Data"})
    columns.append({ "fieldname": "term", "label": "Term", "fieldtype": "Data"})
    columns.append({ "fieldname": "level", "label": "Level", "fieldtype": "Data"})
    columns.append({ "fieldname": "refnum", "label": "Reference Number", "fieldtype": "Data"})
    columns.append({ "fieldname": "amount", "label": "Amount", "fieldtype": "Currency"})
    columns.append({ "fieldname": "date_due", "label": "Date Due", "fieldtype": "Date"})
    columns.append({ "fieldname": "status", "label": "Status", "fieldtype": "Data"})

    dict_student_bills = frappe.get_all("Student Bill", filters=filters, fields=["name", "bill_date", "enrollment_student_first_name","enrollment_student_last_name", "enrollment_level", "enrollment_term", "refnum", "status", "bill_date_due", "total_amount"])

    for dict_student_bill in dict_student_bills:
        row = dict()
        row["date"] = dict_student_bill["bill_date"]
        row["invoice_nb"] = dict_student_bill["name"]
        first_name = dict_student_bill["enrollment_student_first_name"]
        last_name = dict_student_bill["enrollment_student_last_name"]
        row["debtor"] = f"{last_name} {first_name}"
        row["term"] = dict_student_bill["enrollment_term"]
        row["level"] = dict_student_bill["enrollment_level"]
        row["refnum"] = dict_student_bill["refnum"]
        row["date_due"] = dict_student_bill["bill_date_due"]
        row["status"] = dict_student_bill["status"]
        row["amount"] = dict_student_bill["total_amount"]
        data.append(row)

    dict_generic_bills = frappe.get_all("Generic Bill", filters=filters, fields=["name", "debtor_address_name", "debtor_address_street", "debtor_address_number", "debtor_address_city", "debtor_address_zip", "refnum", "status", "amount"])

    for dict_generic_bill in dict_generic_bills:
        row = dict()
        row["invoice_nb"] = dict_generic_bill["name"]
        address_name = dict_generic_bill["debtor_address_name"]
        address_street = dict_generic_bill["debtor_address_street"]
        address_number = dict_generic_bill["debtor_address_number"]
        row["debtor"] = f"{address_name}, {address_street} {address_number}"
        row["refnum"] = dict_generic_bill["refnum"]
        row["amount"] = dict_generic_bill["amount"]
        row["status"] = dict_generic_bill["status"]
        data.append(row)



    return columns, data
