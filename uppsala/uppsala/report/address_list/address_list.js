// Copyright (c) 2016, HFMTS and contributors
// For license information, please see license.txt
/* eslint-disable */

function set_filters() {
  
  //set default for term filter
  frappe.db.get_doc('School Settings').then(settings => {
    console.log(`Setting current term to ${settings.current_term}`);
    frappe.query_reports["Address List"]["filters"][1]["default"] = settings.current_term;
  });
}

set_filters();
