// Copyright (c) 2023, HFMTS and contributors
// For license information, please see license.txt
/* eslint-disable */
function set_filters() {
  
  let filters = [
         {
            "fieldname": "school_class",
            "label": __("School Class"),
            "fieldtype": "Link",
            "options": "School Class",
            "default": "Class20"
        },
        {
            "fieldname": "level_from",
            "label": __("Level Start"),
            "fieldtype": "Link",
            "options": "Level",
            "default": "Sem7"
        },
        {
            "fieldname": "level_to",
            "label": __("Level End"),
            "fieldtype": "Link",
            "options": "Level",
            "default": "Sem7"
        },
        {
            "fieldname": "show_attendance",
            "label": __("Show Attendance"),
            "fieldtype": "Check",
            "default": false
        }

    ];
  
  // set default filters
  frappe.query_reports["Grade Overview"] = {
    "filters": filters,
  };


  // set default for term filter
  // frappe.db.get_doc('School Settings').then(settings => {
  //   console.log(`Setting current term to ${settings.current_term}`);
  //   frappe.query_reports["Grade List"]["filters"][1]["default"] = settings.current_term;
  // });
}

set_filters();
