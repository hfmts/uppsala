# Copyright (c) 2013, HFMTS and contributors
# For license information, please see license.txt

import frappe
from uppsala.uppsala.doctype.level.level import get_passed_levels, get_levels_as_list

frappe.utils.logger.set_log_level("DEBUG")
logger = frappe.logger("uppsala")



def _construct_special_row(dict_course_executions:list[dict], field:str) -> dict:
    row = dict()
    row["first_name"] = ""
    row["last_name"] = ""
    for dict_course_execution in dict_course_executions:
        name = dict_course_execution["name_column"]
        row[name] = dict_course_execution[field]
    return row

def get_unique_by_field(elements:list[dict], field:str) -> list[str]:
    """
        construct a list of all unique values of field in elements
    """
    uniques = list()
    for element in elements:
        if element[field] not in uniques:
            uniques.append(element[field])
    return uniques


def get_course_executions(terms:list[str], levels:list[str], students:None|list[str]=None) -> list[dict]:
    """
        get all course executions for given terms and levels

        If students are supplied, make sure that the student is actually enrolled in given course.
    """
    course_execution_filters = { "term": [ "in", terms], "level": [ "in", levels] }
    dict_course_executions = frappe.get_all("Course Execution", filters=course_execution_filters, fields=["name", "course", "level", "term"], order_by="level asc")
    return dict_course_executions

def add_course_names_short(dict_course_executions:list[dict]) -> list[dict]:

    """
        addes the key name_short to a given dict of course_executions
    """
    for dict_course_execution in dict_course_executions:
        course = frappe.get_doc("Course", dict_course_execution["course"])
        dict_course_execution["name_short"] = f"{course.name_short}"
    return dict_course_executions

def get_assessments(course_execution:dict) -> list[dict]:
    dict_assessments = frappe.get_all("Assessment", filters = { "course_execution": course_execution["name"] }, fields=["name"])
    return dict_assessments

def get_grade(assessment:dict, student:str) -> str:
    assessment_filters = { "parent": assessment["name"], "student": student }
    dict_grades = frappe.get_all("Student Assessment", filters=assessment_filters, fields=["grade_value"])
    grade = ""
    match len(dict_grades):
        case 0: grade = "-"
        case 1: grade = dict_grades[0]["grade_value"]
        case _: grade = "?"
    return grade

def get_student_attendance(attendance:dict, student:str) -> str:
    status = ""
    student_attendance_filters = { "parent": attendance["name"], "student": student }
    dict_student_attendances = frappe.get_all("Student Attendance", filters=student_attendance_filters, fields=["attendance_value"])
    match len(dict_student_attendances):
        case 0: status = "-"
        case 1: status = dict_student_attendances[0]["attendance_value"]
        case _: status = "?"
    return status

def get_attendance(course_execution:dict, student:str) -> str:
    attendance_filters = { "course_execution": course_execution["name"]}
    dict_attendances = frappe.get_all("Attendance", filters=attendance_filters)
    status = ""
    match len(dict_attendances):
        case 0: status = "x"
        case 1: status = get_student_attendance(dict_attendances[0], student)
        case _: status = "?"

    return status

def grade_summary():
    """ 
        get grade summary for given student for enrollment and all previous enrollments 
    
        not yet used, but has been implemented as proof of concept along with grade_overview
    """

    enrollment_id = "ENR-000339"
    enrollment = frappe.get_doc("Enrollment", enrollment_id)
    student_id = enrollment.student

    enrollment_filter = { "student": student_id }
    enrollments:list[dict] = frappe.get_all("Enrollment", filters=enrollment_filter, fields=["name", "student", "term", "level"])

    logger.debug(f"For student {student_id} found {enrollments=}")
   
    # find all passed levels
    levels = get_passed_levels(enrollment.level)
    
    # combine levels and terms
    lvl_trms: list[dict] = list()
    for enrollment in enrollments:
        if enrollment["level"] in levels:
            lvl_trms.append({"level": enrollment["level"], "term": enrollment["term"] })
    
    # go through all level and term combos
    summary:list[dict] = list()
    for lvl_trm in lvl_trms:
        summary_level = dict()
        summary_level["level"] = lvl_trm["level"]
        summary_level["term"] = lvl_trm["term"]
        grades = dict()
        course_executions:list[dict] = get_course_executions(lvl_trm["term"], lvl_trm["level"], students=None)
        course_executions = add_course_names_short(course_executions)

        for course_execution in course_executions:
            assessments:list[dict] = get_assessments(course_execution)
            for assessment in assessments:
                grade = get_grade(assessment, student_id)
                grades[course_execution["name_short"]] = grade
        summary_level["grades"] = grades
        summary.append(summary_level)

    print(student_id)
    print(summary)



def execute(filters=None):
    columns, data = [], []


    # set filters for debuging with bench execute uppsala.uppsala.report.grade_list.grade_list.execute
    # filters = {'school_class': 'Class20', 'level': ['Sem6', 'Sem7']}
    logger.debug(f"Filters: {filters}")
    # check if school_class and term or level filter are set
    if not filters:
        return None
    if not ("school_class" in filters and "level_from" in filters and "level_to" in filters):
        return None
    lvl_from = filters["level_from"] 
    lvl_to = filters["level_to"] 
    if lvl_from == lvl_to:
        levels = [lvl_from]
    else:
        lvls = get_levels_as_list()
        idx_from = lvls.index(lvl_from)
        idx_to = lvls.index(lvl_to)
        levels = lvls[idx_from:idx_to]
    logger.debug(f"{levels=}")
    show_attendance = False
    if "show_attendance" in filters and filters["show_attendance"]:
        show_attendance = True

    ##########################################
    # query database

    # get all enrollments of given levels and school class
    enrollment_filter = { "school_class": filters["school_class"], "level": [ "in", levels] }
    dict_enrollments = frappe.get_all("Enrollment", filters=enrollment_filter, fields=["name", "student", "term"])
    logger.info(f"Found {len(dict_enrollments)} enrollments for given school class and levels")
    
    # extract unique students from all enrollments
    students:list[str] = get_unique_by_field(dict_enrollments, "student")
    terms:list[str] = get_unique_by_field(dict_enrollments, "term")

    logger.info(f"Found {len(terms)} terms and {len(students)} students")

    # get all course executions of given term and levels
    dict_course_executions = get_course_executions(terms, levels)
    logger.info(f"Found {len(dict_course_executions)} course executions for given levels and terms")

    # get short names of courses
    dict_course_executions = add_course_names_short(dict_course_executions)

    # create custom column_name
    for dict_course_execution in dict_course_executions:
        name_short = dict_course_execution["name_short"]
        level = dict_course_execution["level"]
        term = dict_course_execution["term"]
        colname = f"{name_short} ({level}/{term})"
        dict_course_execution["name_col_grade"] = colname
        dict_course_execution["name_col_attendance"] = f"{colname} A"

    grade_overview = dict()
    attendance_overview = dict()
    for student in students:
        grades = dict()
        attendance = dict()
        for dict_course_execution in dict_course_executions:
            # for each assessment get the grades for every student
            dict_assessments = get_assessments(dict_course_execution)
            for dict_assessment in dict_assessments:
                grades[dict_course_execution["name_col_grade"]] = get_grade(dict_assessment, student)
            if show_attendance:
                # for the attendance get the status
                status:str = get_attendance(dict_course_execution, student)
                attendance[dict_course_execution["name_col_attendance"]] = status

        grade_overview[student] = grades   
        if show_attendance:
            attendance_overview[student] = attendance
    
    ##########################################
    # build columns
    columns.append({ "fieldname": "first_name", "label": "First Name", "fieldtype": "Data"})
    columns.append({ "fieldname": "last_name", "label": "Last Name", "fieldtype": "Data"})

    # create columns
    for dict_course_execution in dict_course_executions:
        name_grade = dict_course_execution["name_col_grade"]
        columns.append({ "fieldname": name_grade, "label": name_grade, "fieldtype": "Data"})
        if show_attendance:
            name_attendance = dict_course_execution["name_col_attendance"]
            columns.append({ "fieldname": name_attendance, "label": name_attendance, "fieldtype": "Data"})


    ##########################################
    # build data
    logger.debug(grade_overview)

    # data.append(_construct_special_row(dict_course_executions, "level")) 
    # data.append(_construct_special_row(dict_course_executions, "term")) 

    for student_id in grade_overview:
        student = frappe.get_doc("Student", student_id)
        meta = { "first_name": student.first_name, "last_name": student.last_name }
        if not show_attendance:
            data.append({ **meta, **grade_overview[student_id] })
        else:
            data.append({ **meta, **grade_overview[student_id], **attendance_overview[student_id] })


    
    
    return columns, data


