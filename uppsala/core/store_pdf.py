
import frappe
import frappe.utils
from frappe.www.printview import validate_print_permission
import uppsala.core.utils
from uppsala.core.rpc import RPCResponse, RPCResponseCode
from PyPDF2 import PdfMerger
from pathlib import Path
from io import BytesIO

from dataclasses import dataclass
from enum import Enum
import json

frappe.utils.logger.set_log_level("DEBUG")
logger = frappe.logger("uppsala")

@dataclass
class StorePdfSettings:
    doctype:str
    print_format:str


STORE_PDF_DOCTYPES:list[StorePdfSettings] = [
    StorePdfSettings("Student Bill", "Bill"),      
    StorePdfSettings("Grade Report", "Report Card"),           
    StorePdfSettings("Generic Bill", "Generic Bill"),
    StorePdfSettings("Enrollment Confirmation", "Enrollment Confirmation"),
]

def get_settings_of_doctype(doctype:str) -> StorePdfSettings|None:
    for entry in STORE_PDF_DOCTYPES:
        if entry.doctype == doctype:
            return entry
    return None


def store_pdf_task(doctype:str, item:str, print_format:str):
    logger.info(f"Storing pdf for {doctype} and record {item}")
    doc = frappe.get_doc(doctype, item)

    # for the get_print to work for pdf, make sure that the server is running and
    # the settings site_config.json are set correctly:
    # "hostname": "localhost",
    # "http_port": 8000

    report_pdf = frappe.get_print(doctype=doctype, name=doc.name, print_format=print_format, style="HFMTS 2022", as_pdf=True)
    
    doc_files = frappe.get_all("File", filters = { "attached_to_doctype": doctype, "attached_to_name": doc.name })

    if len(doc_files) > 0:
        for doc_file in doc_files:
            file_old = frappe.get_doc("File", doc_file["name"])
            logger.info(f"Deleting file {file_old.name} - {file_old.file_name} - {file_old.attached_to_doctype} - {file_old.attached_to_name}")
            file_old.delete()

    # store file document
    file_name = f"{doc.name}.pdf"
    doc_file = frappe.get_doc(
    {
        "doctype": "File",
        "file_name": file_name,
        "attached_to_doctype": doctype,
        "attached_to_name": doc.name,
        "is_private": True,
        "content": report_pdf,
    })
    doc_file.save()

    # save last store time
    doc.date_stored = frappe.utils.now()
    doc.save()


@frappe.whitelist()
def store_pdf(doctype:str, items:str) -> RPCResponse:
    """stores custom print formats of doctype for given items

        Note: The pdfs in external file store are handled in the file write handler
                in uppsala/overrides/file.py
    """

    settings = get_settings_of_doctype(doctype)
    
    if settings is None:
        return {"meta": { "code": RPCResponseCode.FAIL, "msg": f"store pdf not supported for doctype {doctype}" }}

    items = json.loads(items)

    for i, item in enumerate(items):
        if len(items) > 1:
            # show progress if more than one item is processed (usually when invoked from list view)
            frappe.publish_progress(float(i+1)/len(items)*100, title='Store PDF', description=f'Processing {item}')
        store_pdf_task(doctype=doctype, item=item, print_format=settings.print_format)

        # could also be run by background workers, however progress update would be required to be implemented
        # frappe.enqueue(store_pdf_task, queue='short', doctype=doctype, item=item, print_format=print_format)

    return {"meta": { "code": RPCResponseCode.SUCCESS }}


@frappe.whitelist()
def merge_pdf_task(doctype:str, items:str) -> RPCResponse:
    """

    """

    settings = get_settings_of_doctype(doctype)
    if not settings:
        msg = f"Merging pdfs not supported for doctype {doctype}."
        logger.error(msg)
        return {"meta": { "code": RPCResponseCode.FAIL, "msg": msg }}

    items = json.loads(items)
    
    doc_names = list()

    for item in items:

        docs = frappe.get_all("File", filters={"attached_to_doctype": doctype, "attached_to_name": item})
        logger.debug(f"{docs=}")
        n = len(docs)
        if len(docs) == 1:
            logger.info(f"Found doc {docs[0]}")
            doc_names.append(docs[0])
        else:
            msg = f"Found {n} files for {doctype} {item}, should be 1!"
            logger.error(msg)
            return {"meta": { "code": RPCResponseCode.FAIL, "msg": msg }}
    
    merger = PdfMerger()

    # get root path without sites part => .parent
    path_root = uppsala.core.utils.get_root_path().parent
    
    for name in doc_names:
        doc = frappe.get_doc("File", name)
        validate_print_permission(doc)

        path:Path = path_root/doc.get_full_path()

        if not path.exists():
              msg = f"File {doc.file_name} not on disk!"
              logger.error(msg)
              return {"meta": { "code": RPCResponseCode.FAIL, "msg": msg }}

        logger.info(f"Loading pdf from {path}")
        merger.append(str(path))
    # Write to bytes_stream
    pdf_content = bytes()

    # write the file
    # merger.write("/workspace/temp/merged.pdf")
    with BytesIO() as bytes_stream:
        merger.write(bytes_stream)
        bytes_stream.seek(0)
        pdf_content = bytes_stream.read()
    merger.close()

    logger.info(f"Merged pdf ({len(pdf_content)} bytes)")
    data = list()
    data.append(pdf_content)
    return {"meta": { "code": RPCResponseCode.SUCCESS }, "data": data}


@frappe.whitelist()
def merge_pdf(doctype:str, items:str):
    """
        merge several stored print reports to one single file.
        Call this from frontend eg.
        http://localhost:8000/api/method/uppsala.core.store_pdf.merge_pdf?doctype=Student%20Bill&items=[%22BILL-000046%22,%20%22BILL-000051%22]
    """

    ret = merge_pdf_task(doctype, items)

    if ret["meta"]["code"] == RPCResponseCode.SUCCESS:
        frappe.local.response.http_status_code = 200
        frappe.local.response.type = "download"
        frappe.local.response.filename = f"merge_{doctype}_{frappe.utils.today()}.pdf"
        frappe.local.response.filecontent = ret["data"][0]
    else: 
        frappe.local.response.http_status_code = 500
        frappe.local.response.message = str(ret)




