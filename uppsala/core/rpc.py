
from typing import TypedDict
from enum import Enum

class RPCResponseCode(int, Enum):
    SUCCESS = 0
    FAIL = 1

class RPCResponseMeta(TypedDict, total=False):
    code:RPCResponseCode
    msg:str

class RPCResponse(TypedDict, total=False):
    meta:RPCResponseMeta
    data:list|dict
