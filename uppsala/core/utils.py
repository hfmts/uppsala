import frappe
from pathlib import Path
from uppsala.core.rpc import RPCResponse, RPCResponseCode

logger = frappe.logger("uppsala")

def get_root_path():
    site_name = frappe.get_site_path()
    path_root = Path("/workspace/frappe-bench/sites")/site_name
    return path_root

def get_files_path(name:str):
    path_root = get_root_path()
    path_files_base = Path("public/files")
    return path_root/path_files_base/name



@frappe.whitelist()
def get_participants(course_execution):
    msg = dict()
    msg["meta"] = dict()
    msg["data"] = list()

    # get enrollment
    # course_execution = frappe.get_doc("Course Execution", self.course_execution)
    course_enrollments = frappe.get_all("Course Enrollment", filters={"course_execution": course_execution}, fields = ["name", "parent"])
    logger.debug(f"Students enrolled in course {course_execution}: {course_enrollments}")
    participants = list()
    for course_enrollment in course_enrollments:
        enrollment = frappe.get_doc("Enrollment", course_enrollment["parent"], fields = ["name", "student", "student_first_name", "student_last_name"])
        logger.debug(f"Processing {enrollment}")
        participants.append({
            "student": enrollment.student, 
            "student_first_name": enrollment.student_first_name,
            "student_last_name": enrollment.student_last_name})

    # sort by last name 
    participants.sort(key=lambda x: x["student_last_name"], reverse=False)
    msg["data"] = participants
    return msg

@frappe.whitelist()
def check_duplicates(items:str, doctype:str, field:str) -> RPCResponse:
    """check if items exist already
        
        Check if document of type doctype exists that has a link field
        containing one of the given items.

        items: Names of linked document. (doctype A)
        doctype: Name of doctype (doctype B) that links to doctype A.
        field: Name of field of doctype B containing link to doctype A.
        
        ```python
        check_duplicates('["ENR-00001", "ENR-00002"]', "Student Bill", "enrollment")
        ```
    """
    import json
    itemlist = json.loads(items)

    data = list()
    for item in itemlist:
        bills = frappe.get_all(doctype, fields=["name"], filters= {field: item["name"]})
        if len(bills) > 0:
            data.append(item["name"])

    return { "meta": { "code": RPCResponseCode.SUCCESS}, "data": data }



def _dict_to_vcard(d):
    from types import SimpleNamespace
    d = SimpleNamespace(**d)
    lines = [
        'BEGIN:VCARD',
        'VERSION:2.1',
        f'N:{d.last_name};{d.first_name}',
        f'FN:{d.first_name} {d.last_name}',
        f'ORG:',
        f'TITLE:{d.salutation}',
        f'EMAIL;PREF;INTERNET:{d.mail_address}',
        f'TEL;WORK;VOICE:{d.phone_number}',
        f'ADR;WORK;PREF:;;{d.address_street} {d.address_number} {d.zip_code} {d.city}',
        f'REV:1',
        'END:VCARD'
    ]
    text = ""
    for line in lines:
        text += line + "\n"
    return text

def export_addressbook():
    path_dir_out = Path("/workspace/export")
    path_dir_out.mkdir(exist_ok=True)
    students = frappe.get_all("Student", fields=["name", "salutation", "first_name", "last_name", "mail_address", "phone_number", "address_street", "address_number", "zip_code", "city"])
    for student in students:
        with open(path_dir_out/f"{student['name']}.vcf", "w") as file:
            file.write(_dict_to_vcard(student))
