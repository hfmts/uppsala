import frappe
import unittest
from uppsala.core.store_pdf import store_pdf, merge_pdf
import json

class TestStorePDF(unittest.TestCase):
    def setUp(self):
        pass

    def test_store_pdf_bad_doctype(self):
        name = "BILL-000051"

        msg = store_pdf("Enrollment", json.dumps([name]))
        print(msg)



    def test_store_pdf(self):
        name = "BILL-000051"

        msg = store_pdf("Student Bill", json.dumps([name]))

        print(msg)

    def test_merge_pdf(self):
        items = ["BILL-000046", "BILL-000051"]

        msg = merge_pdf("Student Bill", json.dumps(items))

        print(msg)
