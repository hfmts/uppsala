
These files are provided by (Creative Tail)[https://www.creativetail.com/22-free-people-icons/] and were taken from (Wikimedia)[https://commons.wikimedia.org/wiki/File:Creative-Tail-People-women.svg].

These files are licensed under the Creative Commons Attribution 4.0 International license. 	

    You are free:

        to share – to copy, distribute and transmit the work
        to remix – to adapt the work

    Under the following conditions:

        attribution – You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
