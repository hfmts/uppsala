from frappe import _

def get_data():
	return [
		{
			"module_name": "Uppsala",
			"color": "green",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Uppsala")
		}
	]
