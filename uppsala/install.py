
import shutil
import os
from pathlib import Path
import frappe

def before_migrate():
    print("Patching frappe")
    path_root = Path(os.getcwd())/".."
    path_src = path_root/"apps/uppsala/uppsala/uppsala/printing/print_style/hfmts_2022"
    path_dst = path_root/"apps/frappe/frappe/printing/print_style/hfmts_2022"
    if path_dst.exists():
        shutil.rmtree(path_dst)
    shutil.copytree(path_src, path_dst)

def after_migrate():
    pass

def upgrade_student_bills_total_amount():
    """
    """
    print("Removing total position and adding it to total description and total amount field")
    bill_names = frappe.get_all("Student Bill", fields=["name"], order_by="name")
    for bill_name in bill_names:
        print(f"Checking record {bill_name['name']}")
        bill = frappe.get_doc("Student Bill", bill_name['name'])
        description = ""
        amount = 0
        idx_total = None
        for i, position in enumerate(bill.positions):
            print(f"Processing {position.description}")
            if position.type == "total":
                idx_total = i
                description = position.description
                amount = position.cost
        if idx_total is not None: 
            del bill.positions[idx_total]
        bill.total_description = description
        bill.total_amount = amount
        bill.save()


def upgrade_student_bills():
    print("Checking field Additional Information of all Student Bill records")
    bills = frappe.get_all("Student Bill", fields=["name", "enrollment", "additional_information"])

    for bill in bills:
        print(f"Checking record {bill['name']}")
        if bill["additional_information"] == None:
            enrollment = frappe.get_doc("Enrollment", bill["enrollment"])
            student = frappe.get_doc("Student", enrollment.student)
            term = frappe.get_doc("Term", enrollment.term)
            level = frappe.get_doc("Level", enrollment.level)

            additional_information = f"{term.name} - {level.description} - {student.first_name} {student.last_name}"

            doc = frappe.get_doc("Student Bill", bill["name"])
            doc.additional_information = additional_information
            doc.save()

            print(f'Setting field {additional_information=}')

