## Uppsala

Schuladministrationssoftware

## Usage

Consult the [software manual](https://hfmts.gitlab.io/uppsala_docs) (german only).

## Installation

Once your frappe environment is set up, install uppsala as follows:

1. `bench get-app https://gitlab.com/hfmts/uppsala.git`
2. `bench install-app uppsala`

## License

MIT
